#Overview
`LectureDoc` is a markdown dialect that facilitates the creation of a script for a lecture and a corresponding slide set out of a single document. The goal is to streamline the creation of closely aligned slides and a script for lectures and presentations. `LectureDoc` takes some inspiration from the Latex Beamer package, but tries to be as lightweight as possible and focuses on the usage of modern web technologies to make improve the overall experience.

#Background Information

The goal is to be as compatible as possible with 
[Markdown](http://daringfireball.net/projects/markdown/syntax#link) and common extensions, 
such as [MultiMarkdown](http://fletcherpenney.net/multimarkdown/), to leverage existing
tool support.

However, some features are added to facilitate the creation of a slide set. In particular
support was added for splitting up a document in multiple files and for being able to 
define slides.

#License
The LectureDoc parser that parses the source document and creates the HTML document is based on Actuarius and is also licensed under the 3-clause BSD license. 

For details see the `Actuarius-LICENSE` and `LICENSE` files that come with the source.

#Implementation Details
LectureDoc is a derivative of the Actuarius Markdown Processor (see <https://github.com/chenkelmann/actuarius>) which is written in Scala using Scala parser combinators. 

To build LectureDoc you need a working installation of [sbt](http://www.scala-sbt.org).

To compile it use: `sbt package`

To test it use: `sbt test`

To build a single jar that contains all required jars and which can be started use: `sbt one-jar`. Afterwards you can execute `LectureDoc` by passing in a document via the system input stream. LectureDoc will then generate an HTML 5 Document.

