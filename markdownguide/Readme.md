#LectureDoc MarkdownGuide

The `LectureDoc` MarkdownGuide is targeted at new users that want to learn how to use `LectureDoc` and create their own documents.

Updating `LectureDoc_MarkdownGuide.HTML` requires including `<link href="Library/LectureDoc/css/LectureDoc-MarkdownGuide.css" type="text/css" rel="stylesheet">` in the header
section of the updated file beneath `<script src="Library/LectureDoc/LectureDoc.js" type="text/javascript"></script>`.
This .CSS contains specific styling changes for the MarkdownGuide.

In addition to this, whenever `lecturedoc\markdown-guide\Library\LectureDoc\Lecturedoc.js is updated, change `mode: "document"` of applicationState to
`mode: "notes"`. That way, opening the generated *.HTML file for the first time results in directly showing the notes mode, the mode best suited for the MarkdownGuide.