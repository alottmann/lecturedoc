




<!DOCTYPE html>
<html>
  <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# object: http://ogp.me/ns/object# article: http://ogp.me/ns/article# profile: http://ogp.me/ns/profile#">
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>highlight.js/docs/css-classes-reference.rst at master · isagalaev/highlight.js · GitHub</title>
    <link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="GitHub" />
    <link rel="fluid-icon" href="https://github.com/fluidicon.png" title="GitHub" />
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-114.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-144.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144.png" />
    <meta property="fb:app_id" content="1401488693436528"/>

      <meta content="@github" name="twitter:site" /><meta content="summary" name="twitter:card" /><meta content="isagalaev/highlight.js" name="twitter:title" /><meta content="highlight.js - Javascript syntax highlighter" name="twitter:description" /><meta content="https://2.gravatar.com/avatar/fdc6c703379d875de38a014cd0d9fea6?d=https%3A%2F%2Fidenticons.github.com%2F1e352edcba54add02100524e9eef6b19.png&amp;r=x&amp;s=400" name="twitter:image:src" />
<meta content="GitHub" property="og:site_name" /><meta content="object" property="og:type" /><meta content="https://2.gravatar.com/avatar/fdc6c703379d875de38a014cd0d9fea6?d=https%3A%2F%2Fidenticons.github.com%2F1e352edcba54add02100524e9eef6b19.png&amp;r=x&amp;s=400" property="og:image" /><meta content="isagalaev/highlight.js" property="og:title" /><meta content="https://github.com/isagalaev/highlight.js" property="og:url" /><meta content="highlight.js - Javascript syntax highlighter" property="og:description" />

    <meta name="hostname" content="github-fe139-cp1-prd.iad.github.net">
    <meta name="ruby" content="ruby 2.1.0p0-github-tcmalloc (87c9373a41) [x86_64-linux]">
    <link rel="assets" href="https://github.global.ssl.fastly.net/">
    <link rel="conduit-xhr" href="https://ghconduit.com:25035/">
    <link rel="xhr-socket" href="/_sockets" />


    <meta name="msapplication-TileImage" content="/windows-tile.png" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="selected-link" value="repo_source" data-pjax-transient />
    <meta content="collector.githubapp.com" name="octolytics-host" /><meta content="collector-cdn.github.com" name="octolytics-script-host" /><meta content="github" name="octolytics-app-id" /><meta content="5FDED353:6131:1A1999A:53160FDA" name="octolytics-dimension-request_id" />
    

    
    
    <link rel="icon" type="image/x-icon" href="/favicon.ico" />

    <meta content="authenticity_token" name="csrf-param" />
<meta content="bn4lZ56er2RDyxnhoGVMoYEEhmdk9stP0ekIcFJnPpk=" name="csrf-token" />

    <link href="https://github.global.ssl.fastly.net/assets/github-2a7a917f436648afe66033805201e523d97bdf7f.css" media="all" rel="stylesheet" type="text/css" />
    <link href="https://github.global.ssl.fastly.net/assets/github2-13030ed4eb82dfe029c0ba6e71747f11afd4a2b9.css" media="all" rel="stylesheet" type="text/css" />
    
    


      <script crossorigin="anonymous" src="https://github.global.ssl.fastly.net/assets/frameworks-75c3712476c7af863873362d0cdf06a94a4b4fa3.js" type="text/javascript"></script>
      <script async="async" crossorigin="anonymous" src="https://github.global.ssl.fastly.net/assets/github-bfb809c1eec00a59d19ebf7b3ef79e3ef2ac724f.js" type="text/javascript"></script>
      
      <meta http-equiv="x-pjax-version" content="471ba6bd10c1b63758a46a43dd2bbda0">

        <link data-pjax-transient rel='permalink' href='/isagalaev/highlight.js/blob/10b9500b67983f0a9c42d8ce8bf8e8c469f7078c/docs/css-classes-reference.rst'>

  <meta name="description" content="highlight.js - Javascript syntax highlighter" />

  <meta content="99931" name="octolytics-dimension-user_id" /><meta content="isagalaev" name="octolytics-dimension-user_login" /><meta content="1213225" name="octolytics-dimension-repository_id" /><meta content="isagalaev/highlight.js" name="octolytics-dimension-repository_nwo" /><meta content="true" name="octolytics-dimension-repository_public" /><meta content="false" name="octolytics-dimension-repository_is_fork" /><meta content="1213225" name="octolytics-dimension-repository_network_root_id" /><meta content="isagalaev/highlight.js" name="octolytics-dimension-repository_network_root_nwo" />
  <link href="https://github.com/isagalaev/highlight.js/commits/master.atom" rel="alternate" title="Recent Commits to highlight.js:master" type="application/atom+xml" />

  </head>


  <body class="logged_out  env-production windows vis-public page-blob tipsy-tooltips">
    <div class="wrapper">
      
      
      
      


      
      <div class="header header-logged-out">
  <div class="container clearfix">

    <a class="header-logo-wordmark" href="https://github.com/">
      <span class="mega-octicon octicon-logo-github"></span>
    </a>

    <div class="header-actions">
        <a class="button primary" href="/join">Sign up</a>
      <a class="button signin" href="/login?return_to=%2Fisagalaev%2Fhighlight.js%2Fblob%2Fmaster%2Fdocs%2Fcss-classes-reference.rst">Sign in</a>
    </div>

    <div class="command-bar js-command-bar  in-repository">

      <ul class="top-nav">
          <li class="explore"><a href="/explore">Explore</a></li>
        <li class="features"><a href="/features">Features</a></li>
          <li class="enterprise"><a href="https://enterprise.github.com/">Enterprise</a></li>
          <li class="blog"><a href="/blog">Blog</a></li>
      </ul>
        <form accept-charset="UTF-8" action="/search" class="command-bar-form" id="top_search_form" method="get">

<input type="text" data-hotkey=" s" name="q" id="js-command-bar-field" placeholder="Search or type a command" tabindex="1" autocapitalize="off"
    
    
      data-repo="isagalaev/highlight.js"
      data-branch="master"
      data-sha="b1ba561284946cda275656da4eaf2301d7e02127"
  >

    <input type="hidden" name="nwo" value="isagalaev/highlight.js" />

    <div class="select-menu js-menu-container js-select-menu search-context-select-menu">
      <span class="minibutton select-menu-button js-menu-target" role="button" aria-haspopup="true">
        <span class="js-select-button">This repository</span>
      </span>

      <div class="select-menu-modal-holder js-menu-content js-navigation-container" aria-hidden="true">
        <div class="select-menu-modal">

          <div class="select-menu-item js-navigation-item js-this-repository-navigation-item selected">
            <span class="select-menu-item-icon octicon octicon-check"></span>
            <input type="radio" class="js-search-this-repository" name="search_target" value="repository" checked="checked" />
            <div class="select-menu-item-text js-select-button-text">This repository</div>
          </div> <!-- /.select-menu-item -->

          <div class="select-menu-item js-navigation-item js-all-repositories-navigation-item">
            <span class="select-menu-item-icon octicon octicon-check"></span>
            <input type="radio" name="search_target" value="global" />
            <div class="select-menu-item-text js-select-button-text">All repositories</div>
          </div> <!-- /.select-menu-item -->

        </div>
      </div>
    </div>

  <span class="help tooltipped tooltipped-s" aria-label="Show command bar help">
    <span class="octicon octicon-question"></span>
  </span>


  <input type="hidden" name="ref" value="cmdform">

</form>
    </div>

  </div>
</div>




          <div class="site" itemscope itemtype="http://schema.org/WebPage">
    
    <div class="pagehead repohead instapaper_ignore readability-menu">
      <div class="container">
        

<ul class="pagehead-actions">


  <li>
    <a href="/login?return_to=%2Fisagalaev%2Fhighlight.js"
    class="minibutton with-count js-toggler-target star-button tooltipped tooltipped-n"
    aria-label="You must be signed in to use this feature" rel="nofollow">
    <span class="octicon octicon-star"></span>Star
  </a>

    <a class="social-count js-social-count" href="/isagalaev/highlight.js/stargazers">
      2,432
    </a>

  </li>

    <li>
      <a href="/login?return_to=%2Fisagalaev%2Fhighlight.js"
        class="minibutton with-count js-toggler-target fork-button tooltipped tooltipped-n"
        aria-label="You must be signed in to fork a repository" rel="nofollow">
        <span class="octicon octicon-git-branch"></span>Fork
      </a>
      <a href="/isagalaev/highlight.js/network" class="social-count">
        447
      </a>
    </li>
</ul>

        <h1 itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="entry-title public">
          <span class="repo-label"><span>public</span></span>
          <span class="mega-octicon octicon-repo"></span>
          <span class="author">
            <a href="/isagalaev" class="url fn" itemprop="url" rel="author"><span itemprop="title">isagalaev</span></a>
          </span>
          <span class="repohead-name-divider">/</span>
          <strong><a href="/isagalaev/highlight.js" class="js-current-repository js-repo-home-link">highlight.js</a></strong>

          <span class="page-context-loader">
            <img alt="Octocat-spinner-32" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
          </span>

        </h1>
      </div><!-- /.container -->
    </div><!-- /.repohead -->

    <div class="container">
      <div class="repository-with-sidebar repo-container new-discussion-timeline js-new-discussion-timeline  ">
        <div class="repository-sidebar clearfix">
            

<div class="sunken-menu vertical-right repo-nav js-repo-nav js-repository-container-pjax js-octicon-loaders">
  <div class="sunken-menu-contents">
    <ul class="sunken-menu-group">
      <li class="tooltipped tooltipped-w" aria-label="Code">
        <a href="/isagalaev/highlight.js" aria-label="Code" class="selected js-selected-navigation-item sunken-menu-item" data-gotokey="c" data-pjax="true" data-selected-links="repo_source repo_downloads repo_commits repo_tags repo_branches /isagalaev/highlight.js">
          <span class="octicon octicon-code"></span> <span class="full-word">Code</span>
          <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>

        <li class="tooltipped tooltipped-w" aria-label="Issues">
          <a href="/isagalaev/highlight.js/issues" aria-label="Issues" class="js-selected-navigation-item sunken-menu-item js-disable-pjax" data-gotokey="i" data-selected-links="repo_issues /isagalaev/highlight.js/issues">
            <span class="octicon octicon-issue-opened"></span> <span class="full-word">Issues</span>
            <span class='counter'>40</span>
            <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>        </li>

      <li class="tooltipped tooltipped-w" aria-label="Pull Requests">
        <a href="/isagalaev/highlight.js/pulls" aria-label="Pull Requests" class="js-selected-navigation-item sunken-menu-item js-disable-pjax" data-gotokey="p" data-selected-links="repo_pulls /isagalaev/highlight.js/pulls">
            <span class="octicon octicon-git-pull-request"></span> <span class="full-word">Pull Requests</span>
            <span class='counter'>23</span>
            <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>


    </ul>
    <div class="sunken-menu-separator"></div>
    <ul class="sunken-menu-group">

      <li class="tooltipped tooltipped-w" aria-label="Pulse">
        <a href="/isagalaev/highlight.js/pulse" aria-label="Pulse" class="js-selected-navigation-item sunken-menu-item" data-pjax="true" data-selected-links="pulse /isagalaev/highlight.js/pulse">
          <span class="octicon octicon-pulse"></span> <span class="full-word">Pulse</span>
          <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>

      <li class="tooltipped tooltipped-w" aria-label="Graphs">
        <a href="/isagalaev/highlight.js/graphs" aria-label="Graphs" class="js-selected-navigation-item sunken-menu-item" data-pjax="true" data-selected-links="repo_graphs repo_contributors /isagalaev/highlight.js/graphs">
          <span class="octicon octicon-graph"></span> <span class="full-word">Graphs</span>
          <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>

      <li class="tooltipped tooltipped-w" aria-label="Network">
        <a href="/isagalaev/highlight.js/network" aria-label="Network" class="js-selected-navigation-item sunken-menu-item js-disable-pjax" data-selected-links="repo_network /isagalaev/highlight.js/network">
          <span class="octicon octicon-git-branch"></span> <span class="full-word">Network</span>
          <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>
    </ul>


  </div>
</div>

              <div class="only-with-full-nav">
                

  

<div class="clone-url open"
  data-protocol-type="http"
  data-url="/users/set_protocol?protocol_selector=http&amp;protocol_type=clone">
  <h3><strong>HTTPS</strong> clone URL</h3>
  <div class="clone-url-box">
    <input type="text" class="clone js-url-field"
           value="https://github.com/isagalaev/highlight.js.git" readonly="readonly">

    <span aria-label="copy to clipboard" class="js-zeroclipboard url-box-clippy minibutton zeroclipboard-button" data-clipboard-text="https://github.com/isagalaev/highlight.js.git" data-copied-hint="copied!"><span class="octicon octicon-clippy"></span></span>
  </div>
</div>

  

<div class="clone-url "
  data-protocol-type="subversion"
  data-url="/users/set_protocol?protocol_selector=subversion&amp;protocol_type=clone">
  <h3><strong>Subversion</strong> checkout URL</h3>
  <div class="clone-url-box">
    <input type="text" class="clone js-url-field"
           value="https://github.com/isagalaev/highlight.js" readonly="readonly">

    <span aria-label="copy to clipboard" class="js-zeroclipboard url-box-clippy minibutton zeroclipboard-button" data-clipboard-text="https://github.com/isagalaev/highlight.js" data-copied-hint="copied!"><span class="octicon octicon-clippy"></span></span>
  </div>
</div>


<p class="clone-options">You can clone with
      <a href="#" class="js-clone-selector" data-protocol="http">HTTPS</a>
      or <a href="#" class="js-clone-selector" data-protocol="subversion">Subversion</a>.
  <span class="help tooltipped tooltipped-n" aria-label="Get help on which URL is right for you.">
    <a href="https://help.github.com/articles/which-remote-url-should-i-use">
    <span class="octicon octicon-question"></span>
    </a>
  </span>
</p>


  <a href="http://windows.github.com" class="minibutton sidebar-button">
    <span class="octicon octicon-device-desktop"></span>
    Clone in Desktop
  </a>

                <a href="/isagalaev/highlight.js/archive/master.zip"
                   class="minibutton sidebar-button"
                   title="Download this repository as a zip file"
                   rel="nofollow">
                  <span class="octicon octicon-cloud-download"></span>
                  Download ZIP
                </a>
              </div>
        </div><!-- /.repository-sidebar -->

        <div id="js-repo-pjax-container" class="repository-content context-loader-container" data-pjax-container>
          


<!-- blob contrib key: blob_contributors:v21:e0cba5acfdd329230144b79b1f17aed4 -->

<p title="This is a placeholder element" class="js-history-link-replace hidden"></p>

<a href="/isagalaev/highlight.js/find/master" data-pjax data-hotkey="t" class="js-show-file-finder" style="display:none">Show File Finder</a>

<div class="file-navigation">
  

<div class="select-menu js-menu-container js-select-menu" >
  <span class="minibutton select-menu-button js-menu-target" data-hotkey="w"
    data-master-branch="master"
    data-ref="master"
    role="button" aria-label="Switch branches or tags" tabindex="0" aria-haspopup="true">
    <span class="octicon octicon-git-branch"></span>
    <i>branch:</i>
    <span class="js-select-button">master</span>
  </span>

  <div class="select-menu-modal-holder js-menu-content js-navigation-container" data-pjax aria-hidden="true">

    <div class="select-menu-modal">
      <div class="select-menu-header">
        <span class="select-menu-title">Switch branches/tags</span>
        <span class="octicon octicon-remove-close js-menu-close"></span>
      </div> <!-- /.select-menu-header -->

      <div class="select-menu-filters">
        <div class="select-menu-text-filter">
          <input type="text" aria-label="Filter branches/tags" id="context-commitish-filter-field" class="js-filterable-field js-navigation-enable" placeholder="Filter branches/tags">
        </div>
        <div class="select-menu-tabs">
          <ul>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="branches" class="js-select-menu-tab">Branches</a>
            </li>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="tags" class="js-select-menu-tab">Tags</a>
            </li>
          </ul>
        </div><!-- /.select-menu-tabs -->
      </div><!-- /.select-menu-filters -->

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="branches">

        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/isagalaev/highlight.js/blob/line-numbers/docs/css-classes-reference.rst"
                 data-name="line-numbers"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="line-numbers">line-numbers</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item selected">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/isagalaev/highlight.js/blob/master/docs/css-classes-reference.rst"
                 data-name="master"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="master">master</a>
            </div> <!-- /.select-menu-item -->
        </div>

          <div class="select-menu-no-results">Nothing to show</div>
      </div> <!-- /.select-menu-list -->

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="tags">
        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/isagalaev/highlight.js/tree/8.0beta/docs/css-classes-reference.rst"
                 data-name="8.0beta"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="8.0beta">8.0beta</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/isagalaev/highlight.js/tree/8.0/docs/css-classes-reference.rst"
                 data-name="8.0"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="8.0">8.0</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/isagalaev/highlight.js/tree/7.5/docs/css-classes-reference.rst"
                 data-name="7.5"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="7.5">7.5</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/isagalaev/highlight.js/tree/7.4/docs/css-classes-reference.rst"
                 data-name="7.4"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="7.4">7.4</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/isagalaev/highlight.js/tree/7.3/docs/css-classes-reference.rst"
                 data-name="7.3"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="7.3">7.3</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/isagalaev/highlight.js/tree/7.2/docs/css-classes-reference.rst"
                 data-name="7.2"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="7.2">7.2</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/isagalaev/highlight.js/tree/7.1/docs/css-classes-reference.rst"
                 data-name="7.1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="7.1">7.1</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/isagalaev/highlight.js/tree/7.0.1/docs/css-classes-reference.rst"
                 data-name="7.0.1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="7.0.1">7.0.1</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/isagalaev/highlight.js/tree/7.0/docs/css-classes-reference.rst"
                 data-name="7.0"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="7.0">7.0</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/isagalaev/highlight.js/tree/6.2/docs/css-classes-reference.rst"
                 data-name="6.2"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="6.2">6.2</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/isagalaev/highlight.js/tree/6.1/docs/css-classes-reference.rst"
                 data-name="6.1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="6.1">6.1</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/isagalaev/highlight.js/tree/6.0beta/docs/css-classes-reference.rst"
                 data-name="6.0beta"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="6.0beta">6.0beta</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/isagalaev/highlight.js/tree/6.0.1/docs/css-classes-reference.rst"
                 data-name="6.0.1"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="6.0.1">6.0.1</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/isagalaev/highlight.js/tree/6.0/docs/css-classes-reference.rst"
                 data-name="6.0"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="6.0">6.0</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/isagalaev/highlight.js/tree/5.14/docs/css-classes-reference.rst"
                 data-name="5.14"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="5.14">5.14</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/isagalaev/highlight.js/tree/5.13/docs/css-classes-reference.rst"
                 data-name="5.13"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="5.13">5.13</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/isagalaev/highlight.js/tree/5.12/docs/css-classes-reference.rst"
                 data-name="5.12"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="5.12">5.12</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/isagalaev/highlight.js/tree/5.11/docs/css-classes-reference.rst"
                 data-name="5.11"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="5.11">5.11</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/isagalaev/highlight.js/tree/5.10/docs/css-classes-reference.rst"
                 data-name="5.10"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="5.10">5.10</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/isagalaev/highlight.js/tree/5.9/docs/css-classes-reference.rst"
                 data-name="5.9"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="5.9">5.9</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/isagalaev/highlight.js/tree/5.8/docs/css-classes-reference.rst"
                 data-name="5.8"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="5.8">5.8</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/isagalaev/highlight.js/tree/5.7/docs/css-classes-reference.rst"
                 data-name="5.7"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="5.7">5.7</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/isagalaev/highlight.js/tree/5.6/docs/css-classes-reference.rst"
                 data-name="5.6"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="5.6">5.6</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/isagalaev/highlight.js/tree/5.5/docs/css-classes-reference.rst"
                 data-name="5.5"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="5.5">5.5</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/isagalaev/highlight.js/tree/5.4/docs/css-classes-reference.rst"
                 data-name="5.4"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="5.4">5.4</a>
            </div> <!-- /.select-menu-item -->
            <div class="select-menu-item js-navigation-item ">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/isagalaev/highlight.js/tree/5.3/docs/css-classes-reference.rst"
                 data-name="5.3"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="5.3">5.3</a>
            </div> <!-- /.select-menu-item -->
        </div>

        <div class="select-menu-no-results">Nothing to show</div>
      </div> <!-- /.select-menu-list -->

    </div> <!-- /.select-menu-modal -->
  </div> <!-- /.select-menu-modal-holder -->
</div> <!-- /.select-menu -->

  <div class="breadcrumb">
    <span class='repo-root js-repo-root'><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/isagalaev/highlight.js" data-branch="master" data-direction="back" data-pjax="true" itemscope="url"><span itemprop="title">highlight.js</span></a></span></span><span class="separator"> / </span><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/isagalaev/highlight.js/tree/master/docs" data-branch="master" data-direction="back" data-pjax="true" itemscope="url"><span itemprop="title">docs</span></a></span><span class="separator"> / </span><strong class="final-path">css-classes-reference.rst</strong> <span aria-label="copy to clipboard" class="js-zeroclipboard minibutton zeroclipboard-button" data-clipboard-text="docs/css-classes-reference.rst" data-copied-hint="copied!"><span class="octicon octicon-clippy"></span></span>
  </div>
</div>


  <div class="commit file-history-tease">
    <img alt="Oleg Efimov" class="main-avatar js-avatar" data-user="77367" height="24" src="https://0.gravatar.com/avatar/5d2c97b75b1c346bb2bb6c756b587479?d=https%3A%2F%2Fidenticons.github.com%2F286128ebe26db08577503bea21351778.png&amp;r=x&amp;s=140" width="24" />
    <span class="author"><a href="/Sannis" rel="author">Sannis</a></span>
    <time class="js-relative-date" data-title-format="YYYY-MM-DD HH:mm:ss" datetime="2014-02-21T00:25:23-08:00" title="2014-02-21 00:25:23">February 21, 2014</time>
    <div class="commit-title">
        <a href="/isagalaev/highlight.js/commit/8ca6b6fda84e086e77d546956d1af3c448fd994a" class="message" data-pjax="true" title="Merge remote-tracking branch &#39;innocenat/x86asm&#39;

Conflicts:
	AUTHORS.en.txt">Merge remote-tracking branch 'innocenat/x86asm'</a>
    </div>

    <div class="participation">
      <p class="quickstat"><a href="#blob_contributors_box" rel="facebox"><strong>12</strong> contributors</a></p>
          <a class="avatar tooltipped tooltipped-s" aria-label="isagalaev" href="/isagalaev/highlight.js/commits/master/docs/css-classes-reference.rst?author=isagalaev"><img alt="Ivan Sagalaev" class=" js-avatar" data-user="99931" height="20" src="https://1.gravatar.com/avatar/fdc6c703379d875de38a014cd0d9fea6?d=https%3A%2F%2Fidenticons.github.com%2F1e352edcba54add02100524e9eef6b19.png&amp;r=x&amp;s=140" width="20" /></a>
    <a class="avatar tooltipped tooltipped-s" aria-label="Sannis" href="/isagalaev/highlight.js/commits/master/docs/css-classes-reference.rst?author=Sannis"><img alt="Oleg Efimov" class=" js-avatar" data-user="77367" height="20" src="https://0.gravatar.com/avatar/5d2c97b75b1c346bb2bb6c756b587479?d=https%3A%2F%2Fidenticons.github.com%2F286128ebe26db08577503bea21351778.png&amp;r=x&amp;s=140" width="20" /></a>
    <a class="avatar tooltipped tooltipped-s" aria-label="dtao" href="/isagalaev/highlight.js/commits/master/docs/css-classes-reference.rst?author=dtao"><img alt="Dan Tao" class=" js-avatar" data-user="409328" height="20" src="https://1.gravatar.com/avatar/3c9c19ca551799cf691fddaae5056e55?d=https%3A%2F%2Fidenticons.github.com%2F56ca7adde571980e45e39e05d9f5e2b5.png&amp;r=x&amp;s=140" width="20" /></a>
    <a class="avatar tooltipped tooltipped-s" aria-label="iElectric" href="/isagalaev/highlight.js/commits/master/docs/css-classes-reference.rst?author=iElectric"><img alt="Domen Kožar" class=" js-avatar" data-user="126339" height="20" src="https://2.gravatar.com/avatar/64798a399a79ed8d34fa83ba0e61c1ac?d=https%3A%2F%2Fidenticons.github.com%2Fcd1db6bdaab0f94ac28022bf20b6d1a6.png&amp;r=x&amp;s=140" width="20" /></a>
    <a class="avatar tooltipped tooltipped-s" aria-label="EricFromCanada" href="/isagalaev/highlight.js/commits/master/docs/css-classes-reference.rst?author=EricFromCanada"><img alt="Eric Knibbe" class=" js-avatar" data-user="3324775" height="20" src="https://0.gravatar.com/avatar/fe1112af6098b395527a8d5aabe92796?d=https%3A%2F%2Fidenticons.github.com%2F7b5ccad265caa1df7054b7eb8cd7089e.png&amp;r=x&amp;s=140" width="20" /></a>
    <a class="avatar tooltipped tooltipped-s" aria-label="innocenat" href="/isagalaev/highlight.js/commits/master/docs/css-classes-reference.rst?author=innocenat"><img alt="innocenat" class=" js-avatar" data-user="2771943" height="20" src="https://2.gravatar.com/avatar/332fd5cadb3c486dd37c828e05facf8e?d=https%3A%2F%2Fidenticons.github.com%2F721ea537bb2161663ebc20a7866b47ac.png&amp;r=x&amp;s=140" width="20" /></a>
    <a class="avatar tooltipped tooltipped-s" aria-label="idleberg" href="/isagalaev/highlight.js/commits/master/docs/css-classes-reference.rst?author=idleberg"><img alt="Jan T. Sott" class=" js-avatar" data-user="1504938" height="20" src="https://1.gravatar.com/avatar/e58c33b56b00d4148810d427a04e21e0?d=https%3A%2F%2Fidenticons.github.com%2F2624284e52af050a617e7d1d665f0915.png&amp;r=x&amp;s=140" width="20" /></a>
    <a class="avatar tooltipped tooltipped-s" aria-label="ronkorving" href="/isagalaev/highlight.js/commits/master/docs/css-classes-reference.rst?author=ronkorving"><img alt="Ron Korving" class=" js-avatar" data-user="631240" height="20" src="https://1.gravatar.com/avatar/283fd5cc523bc957e48597425c82c5e4?d=https%3A%2F%2Fidenticons.github.com%2F81e1a2883fa03659f97ef325f4563abc.png&amp;r=x&amp;s=140" width="20" /></a>
    <a class="avatar tooltipped tooltipped-s" aria-label="sourrust" href="/isagalaev/highlight.js/commits/master/docs/css-classes-reference.rst?author=sourrust"><img alt="Jeremy Hull" class=" js-avatar" data-user="365596" height="20" src="https://2.gravatar.com/avatar/a06bdb0d694c95c6fa63f06c722acd15?d=https%3A%2F%2Fidenticons.github.com%2F4ca1bd3f601ab74ba5b500839563b990.png&amp;r=x&amp;s=140" width="20" /></a>
    <a class="avatar tooltipped tooltipped-s" aria-label="harttle" href="/isagalaev/highlight.js/commits/master/docs/css-classes-reference.rst?author=harttle"><img alt="Jun Yang" class=" js-avatar" data-user="4427974" height="20" src="https://2.gravatar.com/avatar/1c622d8b5f31897e559d866fab95c773?d=https%3A%2F%2Fidenticons.github.com%2Fa33ab96abfd4302375dacee7775ce771.png&amp;r=x&amp;s=140" width="20" /></a>
    <a class="avatar tooltipped tooltipped-s" aria-label="boutil" href="/isagalaev/highlight.js/commits/master/docs/css-classes-reference.rst?author=boutil"><img alt="Cédric Boutillier" class=" js-avatar" data-user="990732" height="20" src="https://1.gravatar.com/avatar/d3eb85977c2f1a9955d324f7b572979d?d=https%3A%2F%2Fidenticons.github.com%2Ffe38401c267b489f5985de35aabd1f84.png&amp;r=x&amp;s=140" width="20" /></a>
    <a class="avatar tooltipped tooltipped-s" aria-label="maximal" href="/isagalaev/highlight.js/commits/master/docs/css-classes-reference.rst?author=maximal"><img alt="MaximAL" class=" js-avatar" data-user="980679" height="20" src="https://1.gravatar.com/avatar/31ceb4e3685510931b4ba4a4effb7c4e?d=https%3A%2F%2Fidenticons.github.com%2Fcc172759a983c0d2f0fa59449f0e4a95.png&amp;r=x&amp;s=140" width="20" /></a>


    </div>
    <div id="blob_contributors_box" style="display:none">
      <h2 class="facebox-header">Users who have contributed to this file</h2>
      <ul class="facebox-user-list">
          <li class="facebox-user-list-item">
            <img alt="Ivan Sagalaev" class=" js-avatar" data-user="99931" height="24" src="https://1.gravatar.com/avatar/fdc6c703379d875de38a014cd0d9fea6?d=https%3A%2F%2Fidenticons.github.com%2F1e352edcba54add02100524e9eef6b19.png&amp;r=x&amp;s=140" width="24" />
            <a href="/isagalaev">isagalaev</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="Oleg Efimov" class=" js-avatar" data-user="77367" height="24" src="https://0.gravatar.com/avatar/5d2c97b75b1c346bb2bb6c756b587479?d=https%3A%2F%2Fidenticons.github.com%2F286128ebe26db08577503bea21351778.png&amp;r=x&amp;s=140" width="24" />
            <a href="/Sannis">Sannis</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="Dan Tao" class=" js-avatar" data-user="409328" height="24" src="https://1.gravatar.com/avatar/3c9c19ca551799cf691fddaae5056e55?d=https%3A%2F%2Fidenticons.github.com%2F56ca7adde571980e45e39e05d9f5e2b5.png&amp;r=x&amp;s=140" width="24" />
            <a href="/dtao">dtao</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="Domen Kožar" class=" js-avatar" data-user="126339" height="24" src="https://2.gravatar.com/avatar/64798a399a79ed8d34fa83ba0e61c1ac?d=https%3A%2F%2Fidenticons.github.com%2Fcd1db6bdaab0f94ac28022bf20b6d1a6.png&amp;r=x&amp;s=140" width="24" />
            <a href="/iElectric">iElectric</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="Eric Knibbe" class=" js-avatar" data-user="3324775" height="24" src="https://0.gravatar.com/avatar/fe1112af6098b395527a8d5aabe92796?d=https%3A%2F%2Fidenticons.github.com%2F7b5ccad265caa1df7054b7eb8cd7089e.png&amp;r=x&amp;s=140" width="24" />
            <a href="/EricFromCanada">EricFromCanada</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="innocenat" class=" js-avatar" data-user="2771943" height="24" src="https://2.gravatar.com/avatar/332fd5cadb3c486dd37c828e05facf8e?d=https%3A%2F%2Fidenticons.github.com%2F721ea537bb2161663ebc20a7866b47ac.png&amp;r=x&amp;s=140" width="24" />
            <a href="/innocenat">innocenat</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="Jan T. Sott" class=" js-avatar" data-user="1504938" height="24" src="https://1.gravatar.com/avatar/e58c33b56b00d4148810d427a04e21e0?d=https%3A%2F%2Fidenticons.github.com%2F2624284e52af050a617e7d1d665f0915.png&amp;r=x&amp;s=140" width="24" />
            <a href="/idleberg">idleberg</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="Ron Korving" class=" js-avatar" data-user="631240" height="24" src="https://1.gravatar.com/avatar/283fd5cc523bc957e48597425c82c5e4?d=https%3A%2F%2Fidenticons.github.com%2F81e1a2883fa03659f97ef325f4563abc.png&amp;r=x&amp;s=140" width="24" />
            <a href="/ronkorving">ronkorving</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="Jeremy Hull" class=" js-avatar" data-user="365596" height="24" src="https://2.gravatar.com/avatar/a06bdb0d694c95c6fa63f06c722acd15?d=https%3A%2F%2Fidenticons.github.com%2F4ca1bd3f601ab74ba5b500839563b990.png&amp;r=x&amp;s=140" width="24" />
            <a href="/sourrust">sourrust</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="Jun Yang" class=" js-avatar" data-user="4427974" height="24" src="https://2.gravatar.com/avatar/1c622d8b5f31897e559d866fab95c773?d=https%3A%2F%2Fidenticons.github.com%2Fa33ab96abfd4302375dacee7775ce771.png&amp;r=x&amp;s=140" width="24" />
            <a href="/harttle">harttle</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="Cédric Boutillier" class=" js-avatar" data-user="990732" height="24" src="https://1.gravatar.com/avatar/d3eb85977c2f1a9955d324f7b572979d?d=https%3A%2F%2Fidenticons.github.com%2Ffe38401c267b489f5985de35aabd1f84.png&amp;r=x&amp;s=140" width="24" />
            <a href="/boutil">boutil</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="MaximAL" class=" js-avatar" data-user="980679" height="24" src="https://1.gravatar.com/avatar/31ceb4e3685510931b4ba4a4effb7c4e?d=https%3A%2F%2Fidenticons.github.com%2Fcc172759a983c0d2f0fa59449f0e4a95.png&amp;r=x&amp;s=140" width="24" />
            <a href="/maximal">maximal</a>
          </li>
      </ul>
    </div>
  </div>

<div class="file-box">
  <div class="file">
    <div class="meta clearfix">
      <div class="info file-name">
        <span class="icon"><b class="octicon octicon-file-text"></b></span>
        <span class="mode" title="File Mode">file</span>
        <span class="meta-divider"></span>
          <span>925 lines (771 sloc)</span>
          <span class="meta-divider"></span>
        <span>31.334 kb</span>
      </div>
      <div class="actions">
        <div class="button-group">
            <a class="minibutton tooltipped tooltipped-w"
               href="http://windows.github.com" aria-label="Open this file in GitHub for Windows">
                <span class="octicon octicon-device-desktop"></span> Open
            </a>
              <a class="minibutton disabled tooltipped tooltipped-w" href="#"
                 aria-label="You must be signed in to make or propose changes">Edit</a>
          <a href="/isagalaev/highlight.js/raw/master/docs/css-classes-reference.rst" class="button minibutton " id="raw-url">Raw</a>
            <a href="/isagalaev/highlight.js/blame/master/docs/css-classes-reference.rst" class="button minibutton js-update-url-with-hash">Blame</a>
          <a href="/isagalaev/highlight.js/commits/master/docs/css-classes-reference.rst" class="button minibutton " rel="nofollow">History</a>
        </div><!-- /.button-group -->
          <a class="minibutton danger disabled empty-icon tooltipped tooltipped-w" href="#"
             aria-label="You must be signed in to make or propose changes">
          Delete
        </a>
      </div><!-- /.actions -->
    </div>
      
  <div id="readme" class="blob instapaper_body">
    <article class="markdown-body entry-content" itemprop="mainContentOfPage"><h1>
<a name="css-classes-reference" class="anchor" href="#css-classes-reference"><span class="octicon octicon-link"></span></a>CSS classes reference</h1>
<p>This is a full list of available classes corresponding to languages'
syntactic structures. The parentheses after language name contain identifiers
used as class names in <tt>&lt;code&gt;</tt> element.</p>
<h2>
<a name="python-python-py-gyp" class="anchor" href="#python-python-py-gyp"><span class="octicon octicon-link"></span></a>Python ("python", "py", "gyp")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>built_in</tt>:         built-in objects (None, False, True and Ellipsis)</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>string</tt>:           string (of any type)</li>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>decorator</tt>:        @-decorator for functions</li>
<li>
<tt>function</tt>:         function header "def some_name(...):"</li>
<li>
<tt>class</tt>:            class header "class SomeName(...):"</li>
<li>
<tt>title</tt>:            name of a function or a class inside a header</li>
<li>
<tt>params</tt>:           everything inside parentheses in a function's or class' header</li>
</ul><h2>
<a name="python-profiler-results-profile" class="anchor" href="#python-profiler-results-profile"><span class="octicon octicon-link"></span></a>Python profiler results ("profile")</h2>
<ul>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>built_in</tt>:         built-in function entry</li>
<li>
<tt>filename</tt>:         filename in an entry</li>
<li>
<tt>summary</tt>:          profiling summary</li>
<li>
<tt>header</tt>:           header of table of results</li>
<li>
<tt>keyword</tt>:          column header</li>
<li>
<tt>function</tt>:         function name in an entry (including parentheses)</li>
<li>
<tt>title</tt>:            actual name of a function in an entry (excluding parentheses)</li>
<li>
<tt>prompt</tt>:           interpreter prompt (&gt;&gt;&gt; or ...)</li>
</ul><h2>
<a name="ruby-ruby-rb-gemspec-podspec-thor" class="anchor" href="#ruby-ruby-rb-gemspec-podspec-thor"><span class="octicon octicon-link"></span></a>Ruby ("ruby", "rb", "gemspec", "podspec", "thor")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>subst</tt>:            in-string substitution (#{...})</li>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>yardoctag</tt>:        YARD tag</li>
<li>
<tt>function</tt>:         function header "def some_name(...):"</li>
<li>
<tt>class</tt>:            class header "class SomeName(...):"</li>
<li>
<tt>title</tt>:            name of a function or a class inside a header</li>
<li>
<tt>parent</tt>:           name of a parent class</li>
<li>
<tt>symbol</tt>:           symbol</li>
</ul><h2>
<a name="haml-haml" class="anchor" href="#haml-haml"><span class="octicon octicon-link"></span></a>Haml ("haml")</h2>
<ul>
<li>
<tt>tag</tt>:              any tag starting with "%"</li>
<li>
<tt>title</tt>:            tag's name</li>
<li>
<tt>attribute</tt>:        tag's attribute</li>
<li>
<tt>keyword</tt>:          tag's attribute that is a keyword</li>
<li>
<tt>string</tt>:           attribute's value that is a string</li>
<li>
<tt>value</tt>:            attribute's value, shorthand id or class for tag</li>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>doctype</tt>:          !!! declaration</li>
<li>
<tt>bullet</tt>:           line defined by variable</li>
</ul><h2>
<a name="perl-perl-pl" class="anchor" href="#perl-perl-pl"><span class="octicon octicon-link"></span></a>Perl ("perl", "pl")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>regexp</tt>:           regular expression</li>
<li>
<tt>sub</tt>:              subroutine header (from "sub" till "{")</li>
<li>
<tt>variable</tt>:         variable starting with "$", "%", "@"</li>
<li>
<tt>operator</tt>:         operator</li>
<li>
<tt>pod</tt>:              plain old doc</li>
</ul><h2>
<a name="php-php-php3-php4-php5-php6" class="anchor" href="#php-php-php3-php4-php5-php6"><span class="octicon octicon-link"></span></a>PHP ("php", "php3", "php4", "php5", "php6")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>string</tt>:           string (of any type)</li>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>phpdoc</tt>:           phpdoc params in comments</li>
<li>
<tt>variable</tt>:         variable starting with "$"</li>
<li>
<tt>preprocessor</tt>:     preprocessor marks: "&lt;?php" and "?&gt;"</li>
</ul><h2>
<a name="scala-scala" class="anchor" href="#scala-scala"><span class="octicon octicon-link"></span></a>Scala ("scala")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>annotation</tt>:       annotation</li>
<li>
<tt>javadoc</tt>:          javadoc comment</li>
<li>
<tt>javadoctag</tt>:       @-tag in javadoc</li>
<li>
<tt>class</tt>:            class header</li>
<li>
<tt>title</tt>:            class name inside a header</li>
<li>
<tt>params</tt>:           everything in parentheses inside a class header</li>
<li>
<tt>inheritance</tt>:      keywords "extends" and "with" inside class header</li>
</ul><h2>
<a name="go-go-golang" class="anchor" href="#go-go-golang"><span class="octicon octicon-link"></span></a>Go ("go", "golang")</h2>
<ul>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>string</tt>:           string constant</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>keyword</tt>:          language keywords</li>
<li>
<tt>constant</tt>:         true false nil iota</li>
<li>
<tt>typename</tt>:         built-in plain types (int, string etc.)</li>
<li>
<tt>built_in</tt>:         built-in functions</li>
</ul><h2>
<a name="html-xml-xml-html-xhtml-rss-atom-xsl-plist" class="anchor" href="#html-xml-xml-html-xhtml-rss-atom-xsl-plist"><span class="octicon octicon-link"></span></a>HTML, XML ("xml", "html", "xhtml", "rss", "atom", "xsl", "plist")</h2>
<ul>
<li>
<tt>tag</tt>:              any tag from "&lt;" till "&gt;"</li>
<li>
<tt>attribute</tt>:        tag's attribute with or without value</li>
<li>
<tt>value</tt>:            attribute's value</li>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>pi</tt>:               processing instruction (&lt;? ... ?&gt;)</li>
<li>
<tt>doctype</tt>:          &lt;!DOCTYPE ... &gt; declaration</li>
<li>
<tt>cdata</tt>:            CDATA section</li>
</ul><h2>
<a name="lasso-lasso-ls-lassoscript" class="anchor" href="#lasso-lasso-ls-lassoscript"><span class="octicon octicon-link"></span></a>Lasso ("lasso", "ls", "lassoscript")</h2>
<ul>
<li>
<tt>preprocessor</tt>:     delimiters and interpreter flags</li>
<li>
<tt>shebang</tt>:          Lasso 9 shell script header</li>
<li>
<tt>comment</tt>:          single- or multi-line comment</li>
<li>
<tt>javadoc</tt>:          doc comment</li>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>literal</tt>:          keyword representing a value</li>
<li>
<tt>built_in</tt>:         built-in types and variables</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>variable</tt>:         variable reference starting with "#" or "$"</li>
<li>
<tt>tag</tt>:              tag literal</li>
<li>
<tt>attribute</tt>:        named or rest parameter in method signature</li>
<li>
<tt>subst</tt>:            unary/binary/ternary operator symbols</li>
<li>
<tt>class</tt>:            type, trait, or method header</li>
<li>
<tt>title</tt>:            name following "define" inside a header</li>
</ul><h2>
<a name="css-css" class="anchor" href="#css-css"><span class="octicon octicon-link"></span></a>CSS ("css")</h2>
<ul>
<li>
<tt>tag</tt>:              tag in selectors</li>
<li>
<tt>id</tt>:               #some_name in selectors</li>
<li>
<tt>class</tt>:            .some_name in selectors</li>
<li>
<tt>at_rule</tt>:          @-rule till first "{" or ";"</li>
<li>
<tt>attr_selector</tt>:    attribute selector (square brackets in a[href^=http://])</li>
<li>
<tt>pseudo</tt>:           pseudo classes and elemens (:after, ::after etc.)</li>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>rules</tt>:            everything from "{" till "}"</li>
<li>
<tt>attribute</tt>:        property name inside a rule</li>
<li>
<tt>value</tt>:            property value inside a rule, from ":" till ";" or till the end of rule block</li>
<li>
<tt>number</tt>:           number within a value</li>
<li>
<tt>string</tt>:           string within a value</li>
<li>
<tt>hexcolor</tt>:         hex color (#FFFFFF) within a value</li>
<li>
<tt>function</tt>:         CSS function within a value</li>
<li>
<tt>important</tt>:        "!important" symbol</li>
</ul><h2>
<a name="scss-scss" class="anchor" href="#scss-scss"><span class="octicon octicon-link"></span></a>SCSS ("scss")</h2>
<ul>
<li>
<tt>tag</tt>:              tag in selectors</li>
<li>
<tt>id</tt>:               #some_name in selectors</li>
<li>
<tt>class</tt>:            .some_name in selectors</li>
<li>
<tt>at_rule</tt>:          @-rule till first "{" or ";"</li>
<li>
<tt>attr_selector</tt>:    attribute selector (square brackets in a[href^=http://])</li>
<li>
<tt>pseudo</tt>:           pseudo classes and elemens (:after, ::after etc.)</li>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>rules</tt>:            everything from "{" till "}"</li>
<li>
<tt>attribute</tt>:        property name inside a rule</li>
<li>
<tt>value</tt>:            property value inside a rule, from ":" till ";" or till the end of rule block</li>
<li>
<tt>number</tt>:           number within a value</li>
<li>
<tt>string</tt>:           string within a value</li>
<li>
<tt>hexcolor</tt>:         hex color (#FFFFFF) within a value</li>
<li>
<tt>function</tt>:         CSS function within a value</li>
<li>
<tt>important</tt>:        "!important" symbol</li>
<li>
<tt>variable</tt>:         variable starting with "$"</li>
<li>
<tt>preprocessor</tt>:     keywords after @</li>
</ul><h2>
<a name="markdown-markdown-md-mkdown-mkd" class="anchor" href="#markdown-markdown-md-mkdown-mkd"><span class="octicon octicon-link"></span></a>Markdown ("markdown", "md", "mkdown", "mkd")</h2>
<ul>
<li>
<tt>header</tt>:            header</li>
<li>
<tt>bullet</tt>:            list bullet</li>
<li>
<tt>emphasis</tt>:          emphasis</li>
<li>
<tt>strong</tt>:            strong emphasis</li>
<li>
<tt>blockquote</tt>:        blockquote</li>
<li>
<tt>code</tt>:              code</li>
<li>
<tt>horizontal_rule</tt>:   horizontal rule</li>
<li>
<tt>link_label</tt>:        link label</li>
<li>
<tt>link_url</tt>:          link url</li>
<li>
<tt>link_reference</tt>:    link reference</li>
</ul><h2>
<a name="asciidoc-asciidoc" class="anchor" href="#asciidoc-asciidoc"><span class="octicon octicon-link"></span></a>AsciiDoc ("asciidoc")</h2>
<ul>
<li>
<tt>header</tt>:            heading</li>
<li>
<tt>bullet</tt>:            list or labeled bullet</li>
<li>
<tt>emphasis</tt>:          emphasis</li>
<li>
<tt>strong</tt>:            strong emphasis</li>
<li>
<tt>blockquote</tt>:        blockquote</li>
<li>
<tt>code</tt>:              inline or block code</li>
<li>
<tt>horizontal_rule</tt>:   horizontal rule</li>
<li>
<tt>link_label</tt>:        link or image label</li>
<li>
<tt>link_url</tt>:          link or image url</li>
<li>
<tt>comment</tt>:           comment</li>
<li>
<tt>attribute</tt>:         document attribute, block attributes</li>
<li>
<tt>label</tt>:             admonition label</li>
</ul><h2>
<a name="django-django-jinja" class="anchor" href="#django-django-jinja"><span class="octicon octicon-link"></span></a>Django ("django", "jinja")</h2>
<ul>
<li>
<tt>keyword</tt>:          HTML tag in HTML, default tags and default filters in templates</li>
<li>
<tt>tag</tt>:              any tag from "&lt;" till "&gt;"</li>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>doctype</tt>:          &lt;!DOCTYPE ... &gt; declaration</li>
<li>
<tt>attribute</tt>:        tag's attribute with or withou value</li>
<li>
<tt>value</tt>:            attribute's value</li>
<li>
<tt>template_tag</tt>:     template tag {% .. %}</li>
<li>
<tt>variable</tt>:         template variable {{ .. }}</li>
<li>
<tt>template_comment</tt>: template comment, both {# .. #} and {% comment %}</li>
<li>
<tt>filter</tt>:           filter from "|" till the next filter or the end of tag</li>
<li>
<tt>argument</tt>:         filter argument</li>
</ul><h2>
<a name="handlebars-handlebars-hbs-htmlhbs-htmlhandlebars" class="anchor" href="#handlebars-handlebars-hbs-htmlhbs-htmlhandlebars"><span class="octicon octicon-link"></span></a>Handlebars ("handlebars", "hbs", "html.hbs", "html.handlebars")</h2>
<ul>
<li>
<tt>expression</tt>:       expression to be evaluated</li>
<li>
<tt>variable</tt>:         variable</li>
<li>
<tt>begin</tt>:-block      the beginning of a block</li>
<li>
<tt>end</tt>:-block        the ending of a block</li>
<li>
<tt>string</tt>:           string</li>
</ul><h2>
<a name="json-json" class="anchor" href="#json-json"><span class="octicon octicon-link"></span></a>JSON ("json")</h2>
<ul>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>literal</tt>:          "true", "false" and "null"</li>
<li>
<tt>string</tt>:           string value</li>
<li>
<tt>attribute</tt>:        name of an object property</li>
<li>
<tt>value</tt>:            value of an object property</li>
</ul><h2>
<a name="mathematica-mathematica-mma" class="anchor" href="#mathematica-mathematica-mma"><span class="octicon octicon-link"></span></a>Mathematica ("mathematica", "mma")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>list</tt>:             a list { .. } - the basic Mma structure</li>
</ul><h2>
<a name="javascript-javascript-js" class="anchor" href="#javascript-javascript-js"><span class="octicon octicon-link"></span></a>JavaScript ("javascript", "js")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>literal</tt>:          special literal: "true", "false" and "null"</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>regexp</tt>:           regular expression</li>
<li>
<tt>function</tt>:         header of a function</li>
<li>
<tt>title</tt>:            name of a function inside a header</li>
<li>
<tt>params</tt>:           parentheses and everything inside them in a function's header</li>
<li>
<tt>pi</tt>:               'use strict' processing instruction</li>
</ul><h2>
<a name="coffeescript-coffeescript-coffee-cson-iced" class="anchor" href="#coffeescript-coffeescript-coffee-cson-iced"><span class="octicon octicon-link"></span></a>CoffeeScript ("coffeescript", "coffee", "cson", "iced")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>literal</tt>:          special literal: "true", "false" and "null"</li>
<li>
<tt>built_in</tt>:         built-in objects and functions ("window", "console", "require", etc...)</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>subst</tt>:            #{ ... } interpolation in double-quoted strings</li>
<li>
<tt>regexp</tt>:           regular expression</li>
<li>
<tt>function</tt>:         header of a function</li>
<li>
<tt>class</tt>:            header of a class</li>
<li>
<tt>title</tt>:            name of a function variable inside a header</li>
<li>
<tt>params</tt>:           parentheses and everything inside them in a function's header</li>
<li>
<tt>property</tt>:         @-property within class and functions</li>
</ul><h2>
<a name="actionscript-actionscript-as" class="anchor" href="#actionscript-actionscript-as"><span class="octicon octicon-link"></span></a>ActionScript ("actionscript", "as")</h2>
<ul>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>keyword</tt>:          keywords</li>
<li>
<tt>literal</tt>:          literal</li>
<li>
<tt>reserved</tt>:         reserved keyword</li>
<li>
<tt>title</tt>:            name of declaration (package, class or function)</li>
<li>
<tt>preprocessor</tt>:     preprocessor directive (import, include)</li>
<li>
<tt>type</tt>:             type of returned value (for functions)</li>
<li>
<tt>package</tt>:          package (named or not)</li>
<li>
<tt>class</tt>:            class/interface</li>
<li>
<tt>function</tt>:         function</li>
<li>
<tt>param</tt>:            params of function</li>
<li>
<tt>rest_arg</tt>:         rest argument of function</li>
</ul><h2>
<a name="vbscript-vbscript-vbs" class="anchor" href="#vbscript-vbscript-vbs"><span class="octicon octicon-link"></span></a>VBScript ("vbscript", "vbs")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>built_in</tt>:         built-in function</li>
</ul><h2>
<a name="vbnet-vbnet-vb" class="anchor" href="#vbnet-vbnet-vb"><span class="octicon octicon-link"></span></a>VB.Net ("vbnet", "vb")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>built_in</tt>:         built-in types</li>
<li>
<tt>literal</tt>:          "true", "false" and "nothing"</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>xmlDocTag</tt>:        xmldoc tag ("'''", "&lt;!--", "--&gt;", "&lt;..&gt;")</li>
<li>
<tt>preprocessor</tt>:     preprocessor directive</li>
</ul><h2>
<a name="protocol-buffers-protobuf" class="anchor" href="#protocol-buffers-protobuf"><span class="octicon octicon-link"></span></a>Protocol Buffers ("protobuf")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>built_in</tt>:         built-in types (e.g., int64, string)</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>literal</tt>:          "true" and "false"</li>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>class</tt>:            message, service, or enum identifier</li>
<li>
<tt>title</tt>:            enum value</li>
<li>
<tt>function</tt>:         RPC call identifier</li>
</ul><h2>
<a name="http-http" class="anchor" href="#http-http"><span class="octicon octicon-link"></span></a>HTTP ("http")</h2>
<ul>
<li>
<tt>request</tt>:          first line of a request</li>
<li>
<tt>status</tt>:           first line of a response</li>
<li>
<tt>attribute</tt>:        header name</li>
<li>
<tt>string</tt>:           header value or query string in a request line</li>
<li>
<tt>number</tt>:           status code</li>
</ul><h2>
<a name="lua-lua" class="anchor" href="#lua-lua"><span class="octicon octicon-link"></span></a>Lua ("lua")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>built_in</tt>:         built-in operator</li>
<li>
<tt>function</tt>:         header of a function</li>
<li>
<tt>title</tt>:            name of a function inside a header</li>
<li>
<tt>params</tt>:           everything inside parentheses in a function's header</li>
<li>
<tt>long_brackets</tt>:    multiline string in [=[ .. ]=]</li>
</ul><h2>
<a name="delphi-delphi" class="anchor" href="#delphi-delphi"><span class="octicon octicon-link"></span></a>Delphi ("delphi")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>comment</tt>:          comment (of any type)</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>function</tt>:         header of a function, procedure, constructor and destructor</li>
<li>
<tt>title</tt>:            name of a function, procedure, constructor or destructor inside a header</li>
<li>
<tt>params</tt>:           everything inside parentheses in a function's header</li>
<li>
<tt>class</tt>:            class' body from "= class" till "end;"</li>
</ul><h2>
<a name="oxygene-oxygene" class="anchor" href="#oxygene-oxygene"><span class="octicon octicon-link"></span></a>Oxygene ("oxygene")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>comment</tt>:          comment (of any type)</li>
<li>
<tt>string</tt>:           string/char</li>
<li>
<tt>function</tt>:         method, destructor, procedure or function</li>
<li>
<tt>title</tt>:            name of a function (inside function)</li>
<li>
<tt>params</tt>:           everything inside parentheses in a function's header</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>class</tt>:            class' body from "= class" till "end;"</li>
</ul><h2>
<a name="java-java-jsp" class="anchor" href="#java-java-jsp"><span class="octicon octicon-link"></span></a>Java ("java", "jsp")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>comment</tt>:          commment</li>
<li>
<tt>annotaion</tt>:        annotation</li>
<li>
<tt>javadoc</tt>:          javadoc comment</li>
<li>
<tt>class</tt>:            class header from "class" till "{"</li>
<li>
<tt>title</tt>:            class or method name</li>
<li>
<tt>params</tt>:           everything in parentheses inside a class header</li>
<li>
<tt>inheritance</tt>:      keywords "extends" and "implements" inside class header</li>
</ul><h2>
<a name="c-cpp-c-h-c-h" class="anchor" href="#c-cpp-c-h-c-h"><span class="octicon octicon-link"></span></a>C++ ("cpp", "c", "h", "c++", "h++")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>string</tt>:           string and character</li>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>preprocessor</tt>:     preprocessor directive</li>
<li>
<tt>stl_container</tt>:    instantiation of STL containers ("vector&lt;...&gt;")</li>
</ul><h2>
<a name="objective-c-objectivec-m-mm-objc-obj-c" class="anchor" href="#objective-c-objectivec-m-mm-objc-obj-c"><span class="octicon octicon-link"></span></a>Objective C ("objectivec", "m", "mm", "objc", "obj-c")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>built_in</tt>:         Cocoa/Cocoa Touch constants and classes</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>preprocessor</tt>:     preprocessor directive</li>
<li>
<tt>class</tt>:            interface/implementation, protocol and forward class declaration</li>
<li>
<tt>title</tt>:            title (id) of interface, implementation, protocol, class</li>
<li>
<tt>variable</tt>:         properties and struct accesors</li>
</ul><h2>
<a name="vala-vala" class="anchor" href="#vala-vala"><span class="octicon octicon-link"></span></a>Vala ("vala")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>class</tt>:            class definitions</li>
<li>
<tt>title</tt>:            in class definition</li>
<li>
<tt>constant</tt>:         ALL_UPPER_CASE</li>
</ul><h2>
<a name="c-cs" class="anchor" href="#c-cs"><span class="octicon octicon-link"></span></a>C# ("cs")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>comment</tt>:          commment</li>
<li>
<tt>xmlDocTag</tt>:        xmldoc tag ("///", "&lt;!--", "--&gt;", "&lt;..&gt;")</li>
<li>
<tt>title</tt>:            title of namespace or class</li>
</ul><h2>
<a name="f-fsharp-fs" class="anchor" href="#f-fsharp-fs"><span class="octicon octicon-link"></span></a>F# ("fsharp", "fs")</h2>
<ul>
<li>
<tt>keywords</tt>:         keyword</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>commment</tt>:         comment</li>
<li>
<tt>class</tt>:            any custom F# type</li>
<li>
<tt>title</tt>:            the name of a custom F# type</li>
<li>
<tt>annotation</tt>:       any attribute</li>
</ul><h2>
<a name="ocaml-ocaml-ml" class="anchor" href="#ocaml-ocaml-ml"><span class="octicon octicon-link"></span></a>OCaml ("ocaml", "ml")</h2>
<ul>
<li>
<tt>keywords</tt>:         keyword</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>commment</tt>:         comment</li>
<li>
<tt>class</tt>:            any custom OCaml type</li>
<li>
<tt>title</tt>:            the name of a custom OCaml type</li>
<li>
<tt>annotation</tt>:       any attribute</li>
</ul><h2>
<a name="d-d" class="anchor" href="#d-d"><span class="octicon octicon-link"></span></a>D ("d")</h2>
<ul>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>string</tt>:           string constant</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>keyword</tt>:          language keywords (including @attributes)</li>
<li>
<tt>constant</tt>:         true false null</li>
<li>
<tt>built_in</tt>:         built-in plain types (int, string etc.)</li>
</ul><h2>
<a name="renderman-rsl-rsl" class="anchor" href="#renderman-rsl-rsl"><span class="octicon octicon-link"></span></a>RenderMan RSL ("rsl")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>string</tt>:           string (including @"..")</li>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>preprocessor</tt>:     preprocessor directive</li>
<li>
<tt>shader</tt>:           sahder keywords</li>
<li>
<tt>shading</tt>:          shading keywords</li>
<li>
<tt>built_in</tt>:         built-in function</li>
</ul><h2>
<a name="renderman-rib-rib" class="anchor" href="#renderman-rib-rib"><span class="octicon octicon-link"></span></a>RenderMan RIB ("rib")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>commands</tt>:         command</li>
</ul><h2>
<a name="maya-embedded-language-mel" class="anchor" href="#maya-embedded-language-mel"><span class="octicon octicon-link"></span></a>Maya Embedded Language ("mel")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>variable</tt>:         variable</li>
</ul><h2>
<a name="sql-sql" class="anchor" href="#sql-sql"><span class="octicon octicon-link"></span></a>SQL ("sql")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword (mostly SQL'92, SQL'99 and T-SQL)</li>
<li>
<tt>literal</tt>:          special literal: "true" and "false"</li>
<li>
<tt>built_in</tt>:         built-in type name</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>string</tt>:           string (of any type: "..", '..', `..`)</li>
<li>
<tt>comment</tt>:          comment</li>
</ul><h2>
<a name="smalltalk-smalltalk-st" class="anchor" href="#smalltalk-smalltalk-st"><span class="octicon octicon-link"></span></a>Smalltalk ("smalltalk", "st")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>comment</tt>:          commment</li>
<li>
<tt>symbol</tt>:           symbol</li>
<li>
<tt>array</tt>:            array</li>
<li>
<tt>class</tt>:            name of a class</li>
<li>
<tt>char</tt>:             char</li>
<li>
<tt>localvars</tt>:        block of local variables</li>
</ul><h2>
<a name="lisp-lisp" class="anchor" href="#lisp-lisp"><span class="octicon octicon-link"></span></a>Lisp ("lisp")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>comment</tt>:          commment</li>
<li>
<tt>variable</tt>:         variable</li>
<li>
<tt>literal</tt>:          b, t and nil</li>
<li>
<tt>list</tt>:             non-quoted list</li>
<li>
<tt>title</tt>:            first symbol in a non-quoted list</li>
<li>
<tt>body</tt>:             remainder of the non-quoted list</li>
<li>
<tt>quoted</tt>:           quoted list, both "(quote .. )" and "'(..)"</li>
</ul><h2>
<a name="clojure-clojure-clj" class="anchor" href="#clojure-clojure-clj"><span class="octicon octicon-link"></span></a>Clojure ("clojure", "clj")</h2>
<ul>
<li>
<tt>comment</tt>:          comments and hints</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>collection</tt>:       collections</li>
<li>
<tt>attribute</tt>:        :keyword</li>
<li>
<tt>title</tt>:            function name (built-in or user defined)</li>
<li>
<tt>built_in</tt>:         built-in function name</li>
</ul><h2>
<a name="ini-ini" class="anchor" href="#ini-ini"><span class="octicon octicon-link"></span></a>Ini ("ini")</h2>
<ul>
<li>
<tt>title</tt>:            title of a section</li>
<li>
<tt>value</tt>:            value of a setting of any type</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>keyword</tt>:          boolean value keyword</li>
</ul><h2>
<a name="apache-apache-apacheconf" class="anchor" href="#apache-apache-apacheconf"><span class="octicon octicon-link"></span></a>Apache ("apache", "apacheconf")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>comment</tt>:          commment</li>
<li>
<tt>literal</tt>:          On and Off</li>
<li>
<tt>sqbracket</tt>:        variables in rewrites "%{..}"</li>
<li>
<tt>cbracket</tt>:         options in rewrites "[..]"</li>
<li>
<tt>tag</tt>:              begin and end of a configuration section</li>
</ul><h2>
<a name="nginx-nginx-nginxconf" class="anchor" href="#nginx-nginx-nginxconf"><span class="octicon octicon-link"></span></a>Nginx ("nginx", "nginxconf")</h2>
<ul>
<li>
<tt>title</tt>:            directive title</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>built_in</tt>:         built-in constant</li>
<li>
<tt>variable</tt>:         $-variable</li>
<li>
<tt>regexp</tt>:           regexp</li>
</ul><h2>
<a name="diff-diff-patch" class="anchor" href="#diff-diff-patch"><span class="octicon octicon-link"></span></a>Diff ("diff", "patch")</h2>
<ul>
<li>
<tt>header</tt>:           file header</li>
<li>
<tt>chunk</tt>:            chunk header within a file</li>
<li>
<tt>addition</tt>:         added lines</li>
<li>
<tt>deletion</tt>:         deleted lines</li>
<li>
<tt>change</tt>:           changed lines</li>
</ul><h2>
<a name="dos-dos-bat-cmd" class="anchor" href="#dos-dos-bat-cmd"><span class="octicon octicon-link"></span></a>DOS ("dos", "bat", "cmd")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>flow</tt>:             batch control keyword</li>
<li>
<tt>stream</tt>:           DOS special files ("con", "prn", ...)</li>
<li>
<tt>winutils</tt>:         some commands (see dos.js specifically)</li>
<li>
<tt>envvar</tt>:           environment variables</li>
</ul><h2>
<a name="bash-bash-sh-zsh" class="anchor" href="#bash-bash-sh-zsh"><span class="octicon octicon-link"></span></a>Bash ("bash", "sh", "zsh")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>literal</tt>:          special literal: "true" and "false"</li>
<li>
<tt>variable</tt>:         variable</li>
<li>
<tt>shebang</tt>:          script interpreter header</li>
</ul><h2>
<a name="makefile-makefile-mk-mak" class="anchor" href="#makefile-makefile-mk-mak"><span class="octicon octicon-link"></span></a>Makefile ("makefile", "mk", "mak")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword ".PHONY" within the phony line</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>variable</tt>:         $(..) variable</li>
<li>
<tt>title</tt>:            target title</li>
<li>
<tt>constant</tt>:         constant within the initial definition</li>
</ul><h2>
<a name="cmake-cmake-cmakein" class="anchor" href="#cmake-cmake-cmakein"><span class="octicon octicon-link"></span></a>CMake ("cmake", "cmake.in")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>comment</tt>:          commment</li>
<li>
<tt>envvar</tt>:           $-variable</li>
<li>
<tt>operator</tt>:         operator (LESS, STREQUAL, MATCHES, etc)</li>
</ul><h2>
<a name="nix-nix" class="anchor" href="#nix-nix"><span class="octicon octicon-link"></span></a>Nix ("nix")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>built_in</tt>:         built-in constant</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>string</tt>:           single and double quotes</li>
<li>
<tt>subst</tt>:            antiquote ${}</li>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>variable</tt>:         function parameter name</li>
</ul><h2>
<a name="nsis-nsis" class="anchor" href="#nsis-nsis"><span class="octicon octicon-link"></span></a>NSIS ("nsis")</h2>
<ul>
<li>
<tt>symbol</tt>:           directory constants</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>constant</tt>:         definitions, language-strings, compiler commands</li>
<li>
<tt>variable</tt>:         $-variable</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>comment</tt>:          commment</li>
<li>
<tt>params</tt>:           parameters</li>
<li>
<tt>keyword</tt>:          keywords</li>
<li>
<tt>literal</tt>:          keyword options</li>
</ul><h2>
<a name="axapta-axapta" class="anchor" href="#axapta-axapta"><span class="octicon octicon-link"></span></a>Axapta ("axapta")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>comment</tt>:          commment</li>
<li>
<tt>class</tt>:            class header from "class" till "{"</li>
<li>
<tt>title</tt>:            class name inside a header</li>
<li>
<tt>params</tt>:           everything in parentheses inside a class header</li>
<li>
<tt>inheritance</tt>:      keywords "extends" and "implements" inside class header</li>
<li>
<tt>preprocessor</tt>:     preprocessor directive</li>
</ul><h2>
<a name="oracle-rules-language-ruleslanguage" class="anchor" href="#oracle-rules-language-ruleslanguage"><span class="octicon octicon-link"></span></a>Oracle Rules Language ("ruleslanguage")</h2>
<ul>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>string</tt>:           string constant</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>keyword</tt>:          language keywords</li>
<li>
<tt>built_in</tt>:         built-in functions</li>
<li>
<tt>array</tt>:            array stem</li>
</ul><h2>
<a name="1c-1c" class="anchor" href="#1c-1c"><span class="octicon octicon-link"></span></a>1C ("1c")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>date</tt>:             date</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>comment</tt>:          commment</li>
<li>
<tt>function</tt>:         header of function or procudure</li>
<li>
<tt>title</tt>:            function name inside a header</li>
<li>
<tt>params</tt>:           everything in parentheses inside a function header</li>
<li>
<tt>preprocessor</tt>:     preprocessor directive</li>
</ul><h2>
<a name="x86-assembly-x86asm" class="anchor" href="#x86-assembly-x86asm"><span class="octicon octicon-link"></span></a>x86 Assembly ("x86asm")</h2>
<ul>
<li>
<tt>keyword</tt>:          instuction mnemonic</li>
<li>
<tt>literal</tt>:          register name</li>
<li>
<tt>pseudo</tt>:           assembler's pseudo instruction</li>
<li>
<tt>preprocessor</tt>:     macro</li>
<li>
<tt>built_in</tt>:         assembler's keyword</li>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>label</tt>:            jump label</li>
<li>
<tt>argument</tt>:         macro's argument</li>
</ul><h2>
<a name="avr-assembler-avrasm" class="anchor" href="#avr-assembler-avrasm"><span class="octicon octicon-link"></span></a>AVR assembler ("avrasm")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>built_in</tt>:         pre-defined register</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>comment</tt>:          commment</li>
<li>
<tt>label</tt>:            label</li>
<li>
<tt>preprocessor</tt>:     preprocessor directive</li>
<li>
<tt>localvars</tt>:        substitution in .macro</li>
</ul><h2>
<a name="vhdl-vhdl" class="anchor" href="#vhdl-vhdl"><span class="octicon octicon-link"></span></a>VHDL ("vhdl")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>comment</tt>:          commment</li>
<li>
<tt>literal</tt>:          signal logical value</li>
<li>
<tt>typename</tt>:         typename</li>
<li>
<tt>attribute</tt>:        signal attribute</li>
</ul><h2>
<a name="parser3-parser3" class="anchor" href="#parser3-parser3"><span class="octicon octicon-link"></span></a>Parser3 ("parser3")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>comment</tt>:          commment</li>
<li>
<tt>variable</tt>:         variable starting with "$"</li>
<li>
<tt>preprocessor</tt>:     preprocessor directive</li>
<li>
<tt>title</tt>:            user-defined name starting with "@"</li>
</ul><h2>
<a name="livecode-server-livecodeserver" class="anchor" href="#livecode-server-livecodeserver"><span class="octicon octicon-link"></span></a>LiveCode Server ("livecodeserver")</h2>
<ul>
<li>
<tt>variable</tt>:         variable starting with "g", "t", "p", "s", "$_"</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>title</tt>:            name of a command or a function</li>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>constant</tt>:         constant</li>
<li>
<tt>operator</tt>:         operator</li>
<li>
<tt>built_in</tt>:         built_in functions and commands</li>
<li>
<tt>function</tt>:         header of a function</li>
<li>
<tt>command</tt>:          header of a command</li>
<li>
<tt>preprocessor</tt>:     preprocessor marks: "&lt;?", "&lt;?rev", "&lt;?lc", "&lt;?livecode" and "?&gt;"</li>
</ul><h2>
<a name="tex-tex" class="anchor" href="#tex-tex"><span class="octicon octicon-link"></span></a>TeX ("tex")</h2>
<ul>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>command</tt>:          command</li>
<li>
<tt>parameter</tt>:        parameter</li>
<li>
<tt>formula</tt>:          formula</li>
<li>
<tt>special</tt>:          special symbol</li>
</ul><h2>
<a name="haskell-haskell-hs" class="anchor" href="#haskell-haskell-hs"><span class="octicon octicon-link"></span></a>Haskell ("haskell", "hs")</h2>
<ul>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>pragma</tt>:           GHC pragma</li>
<li>
<tt>preprocessor</tt>:     CPP preprocessor directive</li>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>title</tt>:            function or variable name</li>
<li>
<tt>type</tt>:             value, type or type class constructor name (i.e. capitalized)</li>
<li>
<tt>container</tt>:        (..., ...) or {...; ...} list in declaration or record</li>
<li>
<tt>module</tt>:           module declaration</li>
<li>
<tt>import</tt>:           import declaration</li>
<li>
<tt>class</tt>:            type class or instance declaration</li>
<li>
<tt>typedef</tt>:          type declaration (type, newtype, data)</li>
<li>
<tt>default</tt>:          default declaration</li>
<li>
<tt>infix</tt>:            infix declaration</li>
<li>
<tt>foreign</tt>:          FFI declaration</li>
<li>
<tt>shebang</tt>:          shebang line</li>
</ul><h2>
<a name="erlang-erlang-erl" class="anchor" href="#erlang-erlang-erl"><span class="octicon octicon-link"></span></a>Erlang ("erlang", "erl")</h2>
<ul>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>record_name</tt>:      record access (#record_name)</li>
<li>
<tt>title</tt>:            name of declaration function</li>
<li>
<tt>variable</tt>:         variable (starts with capital letter or with _)</li>
<li>
<tt>pp</tt>:.keywords      module's attribute (-attribute)</li>
<li>
<tt>function_name</tt>:    atom or atom:atom in case of function call</li>
</ul><h2>
<a name="elixir-elixir" class="anchor" href="#elixir-elixir"><span class="octicon octicon-link"></span></a>Elixir ("elixir")</h2>
<ul>
<li>
<tt>keyword</tt>:         keyword</li>
<li>
<tt>string</tt>:          string</li>
<li>
<tt>subst</tt>:           in-string substitution (#{...})</li>
<li>
<tt>comment</tt>:         comment</li>
<li>
<tt>function</tt>:        function header "def some_name(...):"</li>
<li>
<tt>class</tt>:           defmodule and defrecord headers</li>
<li>
<tt>title</tt>:           name of a function or a module inside a header</li>
<li>
<tt>symbol</tt>:          atom</li>
<li>
<tt>constant</tt>:        name of a module</li>
<li>
<tt>number</tt>:          number</li>
<li>
<tt>variable</tt>:        variable</li>
<li>
<tt>regexp</tt>:          regexp</li>
</ul><h2>
<a name="rust-rust-rs" class="anchor" href="#rust-rust-rs"><span class="octicon octicon-link"></span></a>Rust ("rust", "rs")</h2>
<ul>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>title</tt>:            name of declaration</li>
<li>
<tt>preprocessor</tt>:     preprocessor directive</li>
</ul><h2>
<a name="matlab-matlab" class="anchor" href="#matlab-matlab"><span class="octicon octicon-link"></span></a>Matlab ("matlab")</h2>
<ul>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>title</tt>:            function name</li>
<li>
<tt>function</tt>:         function</li>
<li>
<tt>param</tt>:            params of function</li>
<li>
<tt>matrix</tt>:           matrix in [ .. ]</li>
<li>
<tt>cell</tt>:             cell in { .. }</li>
</ul><h2>
<a name="scilab-scilab-sci" class="anchor" href="#scilab-scilab-sci"><span class="octicon octicon-link"></span></a>Scilab ("scilab", "sci")</h2>
<ul>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>title</tt>:            function name</li>
<li>
<tt>function</tt>:         function</li>
<li>
<tt>param</tt>:            params of function</li>
<li>
<tt>matrix</tt>:           matrix in [ .. ]</li>
</ul><h2>
<a name="r-r" class="anchor" href="#r-r"><span class="octicon octicon-link"></span></a>R ("r")</h2>
<ul>
<li>
<p><tt>comment</tt>:          comment</p>
</li>
<li>
<p><tt>string</tt>:           string constant</p>
</li>
<li>
<p><tt>number</tt>:           number</p>
</li>
<li><dl>
<dt>
<tt>keyword</tt>:          language keywords (function, if) plus "structural"</dt>
<dd>
<p>functions (attach, require, setClass)</p>
</dd>
</dl></li>
<li>
<p><tt>literal</tt>:          special literal: TRUE, FALSE, NULL, NA, etc.</p>
</li>
</ul><h2>
<a name="opengl-shading-language-glsl" class="anchor" href="#opengl-shading-language-glsl"><span class="octicon octicon-link"></span></a>OpenGL Shading Language ("glsl")</h2>
<ul>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>preprocessor</tt>:     preprocessor directive</li>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>built_in</tt>:         GLSL built-in functions and variables</li>
<li>
<tt>literal</tt>:          true false</li>
</ul><h2>
<a name="applescript-applescript-osascript" class="anchor" href="#applescript-applescript-osascript"><span class="octicon octicon-link"></span></a>AppleScript ("applescript", "osascript")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>command</tt>:          core AppleScript command</li>
<li>
<tt>constant</tt>:         AppleScript built in constant</li>
<li>
<tt>type</tt>:             AppleScript variable type (integer, etc.)</li>
<li>
<tt>property</tt>:         Applescript built in property (length, etc.)</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>title</tt>:            name of a handler</li>
</ul><h2>
<a name="vim-script-vim" class="anchor" href="#vim-script-vim"><span class="octicon octicon-link"></span></a>Vim Script ("vim")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>built_in</tt>:         built-in functions</li>
<li>
<tt>string</tt>:           string, comment</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>function</tt>:         function header "function Foo(...)"</li>
<li>
<tt>title</tt>:            name of a function</li>
<li>
<tt>params</tt>:           everything inside parentheses in a function's header</li>
<li>
<tt>variable</tt>:         vim variables with different visibilities "g:foo, b:bar"</li>
</ul><h2>
<a name="brainfuck-brainfuck-bf" class="anchor" href="#brainfuck-brainfuck-bf"><span class="octicon octicon-link"></span></a>Brainfuck ("brainfuck", "bf")</h2>
<ul>
<li>
<tt>title</tt>:            Brainfuck while loop command</li>
<li>
<tt>literal</tt>:          Brainfuck inc and dec commands</li>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>string</tt>:           Brainfuck input and output commands</li>
</ul><h2>
<a name="mizar-mizar" class="anchor" href="#mizar-mizar"><span class="octicon octicon-link"></span></a>Mizar ("mizar")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>comment</tt>:          comment</li>
</ul><h2>
<a name="autohotkey-autohotkey" class="anchor" href="#autohotkey-autohotkey"><span class="octicon octicon-link"></span></a>AutoHotkey ("autohotkey")</h2>
<ul>
<li>
<tt>keyword</tt>:          keyword</li>
<li>
<tt>literal</tt>:          A (active window), true, false, NOT, AND, OR</li>
<li>
<tt>built_in</tt>:         built-in variables</li>
<li>
<tt>string</tt>:           string</li>
<li>
<tt>comment</tt>:          comment</li>
<li>
<tt>number</tt>:           number</li>
<li>
<tt>var_expand</tt>:       variable expansion (enclosed in percent sign)</li>
<li>
<tt>label</tt>:            label, hotkey label, hotstring label</li>
</ul><h2>
<a name="fix-fix" class="anchor" href="#fix-fix"><span class="octicon octicon-link"></span></a>FIX ("fix")</h2>
<ul>
<li>
<tt>attribute</tt>:        attribute name</li>
<li>
<tt>string</tt>:           attribute value</li>
</ul></article>
  </div>

  </div>
</div>

<a href="#jump-to-line" rel="facebox[.linejump]" data-hotkey="l" class="js-jump-to-line" style="display:none">Jump to Line</a>
<div id="jump-to-line" style="display:none">
  <form accept-charset="UTF-8" class="js-jump-to-line-form">
    <input class="linejump-input js-jump-to-line-field" type="text" placeholder="Jump to line&hellip;" autofocus>
    <button type="submit" class="button">Go</button>
  </form>
</div>

        </div>

      </div><!-- /.repo-container -->
      <div class="modal-backdrop"></div>
    </div><!-- /.container -->
  </div><!-- /.site -->


    </div><!-- /.wrapper -->

      <div class="container">
  <div class="site-footer">
    <ul class="site-footer-links right">
      <li><a href="https://status.github.com/">Status</a></li>
      <li><a href="http://developer.github.com">API</a></li>
      <li><a href="http://training.github.com">Training</a></li>
      <li><a href="http://shop.github.com">Shop</a></li>
      <li><a href="/blog">Blog</a></li>
      <li><a href="/about">About</a></li>

    </ul>

    <a href="/">
      <span class="mega-octicon octicon-mark-github" title="GitHub"></span>
    </a>

    <ul class="site-footer-links">
      <li>&copy; 2014 <span title="0.02675s from github-fe139-cp1-prd.iad.github.net">GitHub</span>, Inc.</li>
        <li><a href="/site/terms">Terms</a></li>
        <li><a href="/site/privacy">Privacy</a></li>
        <li><a href="/security">Security</a></li>
        <li><a href="/contact">Contact</a></li>
    </ul>
  </div><!-- /.site-footer -->
</div><!-- /.container -->


    <div class="fullscreen-overlay js-fullscreen-overlay" id="fullscreen_overlay">
  <div class="fullscreen-container js-fullscreen-container">
    <div class="textarea-wrap">
      <textarea name="fullscreen-contents" id="fullscreen-contents" class="js-fullscreen-contents" placeholder="" data-suggester="fullscreen_suggester"></textarea>
    </div>
  </div>
  <div class="fullscreen-sidebar">
    <a href="#" class="exit-fullscreen js-exit-fullscreen tooltipped tooltipped-w" aria-label="Exit Zen Mode">
      <span class="mega-octicon octicon-screen-normal"></span>
    </a>
    <a href="#" class="theme-switcher js-theme-switcher tooltipped tooltipped-w"
      aria-label="Switch themes">
      <span class="octicon octicon-color-mode"></span>
    </a>
  </div>
</div>



    <div id="ajax-error-message" class="flash flash-error">
      <span class="octicon octicon-alert"></span>
      <a href="#" class="octicon octicon-remove-close close js-ajax-error-dismiss"></a>
      Something went wrong with that request. Please try again.
    </div>

  </body>
</html>

