#LectureDoc

![LectureDoc - Logo](Library/LectureDoc/Logo@0.25x.png)

~~~[History]slide
 
LectureDoc
===

This document describes and demonstrates the features of _LectureDoc_ – a (multi-)markdown inspired mark-up language to enable the generation of a slide-set and a script out of a single tightly-integrated document. 

To leverage existing tool support for Markdown (dialects), LectureDoc tries to reuse all existing features and tries to add as few new features as possible. Some minor deviations were, however, necessary to facilitate the generation of slides.

<svg id="Development-of-LectureDoc-Timeline" class="timeline"/>
<script type="text/javascript">
	Timeline.draw("Development-of-LectureDoc-Timeline",[
		{d:"2011",t:"First ideas"},
		{d:"2012",t:"HTML based prototype"},
		{d:"2013",t:"LectureDoc"},
	]);	
</script>

~~~

~~~[Sections and Slides]slide

\~\~\~


~~~aside

Craft vs. Engineering
===

Experience can lead us in the right direction. This is craft. Experience will only take us so far into uncharted territory. Then we must take what we started with and make it better through a controlled process of refinement. This is engineering.
~~~

~~~


~~~[Basic Mark-up]slide

Basics
===

* Strong: to make some text bold, enclose the text in "\*\*"s.  
E.g., **\*\*bold\*\***. 
* Emphasize: to emphasize some text, enclose the text in "\_"s.  
E.g., _\_emphasize\__.  
* Horizontal ruler: create a paragraph that contains just three ("-")s; i.e., create an empty line, followed by a line with three dashes, followed by an empty line.

~~~

~~~[Lists]slide
Lists
===

**The report effect:**

1. The customers are not entirely aware of what the computing systems will do for them; they see new requirements as soon as old ones are met.
2. They see one kind of produced report and ask whether the system can also do this and that. 

~~~

~~~[Embedding Images]slide
Embedding Images
====

* Inline:  
**\!\[Description\]\(RelativePath\)**
* Reference-style:  
**\!\[Description\]\[id\]**  
**\[id\]: RelativePath**  
_If you add whitespace after the relative path the line is treated as a block of text that continues in the next line!_

**Example**  
Embedding the logo \!\[LectureDoc - small logo\]\[LD-small\_logo\] in some text.  
\[LD-small\_logo\]: Library/LectureDoc/Logo@0.25x.png

will render as:

Embedding the logo ![LectureDoc - small logo][LD-small_logo] in some text.
[LD-small_logo]: Library/LectureDoc/Logo@0.25x.png

~~~

~~~[Embedding Code]slide
Embedding Code
===

To embed syntax highlighted code, use a fenced code block:

\`\`\`_Language_  
**Code**
\`\`\`

In general, the language is automatically detected, but in case of very short code snippets it is highly recommended that the language is explicitly specified.

**Example**

\`\`\`java  
public class X {}  
\`\`\`

will render as:

```java
public class X {}
```

~~~


~~~[Headings]slide
Headings
===

* Headings: nth-level: (#)n  
\#Heading Level 1  
\#\#Heading Level 2  
* First-Level 

**Best practice:** Use atx-style headers for the overall document and use setext-style headers for slides. This makes it easier to read the document because the types of headers are not confused.

~~~



~~~[Block-Quotes]slide
Block-Quotes
===

>[The major cause of the software crisis is] that the machines have become several orders of magnitude more powerful! To put it quite bluntly: as long as there were no machines, programming was no problem at all; when we had a few weak computers, programming became a mild problem, and now we have gigantic computers, programming has become an equally gigantic problem.
>
>E. Dijkstra, 1972 [#Dijkstra:1972:HP:355604.361591]

~~~



~~~[Embedding HTML Fragments]slide 

Embedding HTML
===

<table>
	<tr><th>Hardware Design</th><th>Software Design</th></tr>
	<tr><td>Product is a physical object</td><td>Product is the running software</td></tr>
	<tr><td>
		Building the product is: 
		<ul>
			<li>done by humans and robots</li>
			<li>expensive </li>
			<li>slow</li>
			<li>hard to redo</li>			
		</ul>
		</td>
		<td>Building the product is: 
	 	<ul>
			<li>done by compilers and linkers </li>
			<li>extremely cheap</li>
			<li>very fast</li>
			<li>easy to redo</li>						
		</td>
	</tr>
	<tr><td>Precise quality measures</td><td>No precise quality measures</td></tr>
</table>

~~~



~~~[Indocument Graphics]slide

Creating a graphic using SVG.
=== 

<svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="100%" style="font-size: 175%; font-family: Courier New;">
	<defs>
	   <marker id='head' orient='auto' markerWidth='10' markerHeight='10' refX='0.1' refY='2'>
	     <path d='M0,0 V4 L2,2 Z' fill='black' />
	   </marker>
	 </defs> 
    <text x="50" y="50" fill="#000066">Object-oriented<tspan x="50" y="80">Programming</tspan></text>
	<line x1="500" y1="170" x2="300" y2="100" style="stroke:rgb(0,0,100);stroke-width:8"  marker-end="url(#head)"/>
    <text x="650" y="50" fill="#000066">Functional<tspan x="650" y="80">Programming</tspan></text>	
	<line x1="500" y1="170" x2="700" y2="100" style="stroke:rgb(0,0,100);stroke-width:8"  marker-end="url(#head)"/>
    <text x="300" y="200" fill="#000066">Modular Programming</text>	
	<line x1="500" y1="320" x2="500" y2="220" style="stroke:rgb(0,0,100);stroke-width:8"  marker-end="url(#head)"/>
    <text x="300" y="350" fill="#000066">Structured Programming</text>
	<line x1="500" y1="470" x2="500" y2="370" style="stroke:rgb(0,0,100);stroke-width:8"  marker-end="url(#head)"/>
    <text x="300" y="500" fill="#000066">Assembler-like Languages	</text>
</svg>

~~~


~~~[Landmark Statements]slide
Complexity and Change are Invariants
===

^Designing for organizing complexity and facilitating change 
is the key to support maintainability. 


~~~ 




~~~[References]slide
References
===

~~~



