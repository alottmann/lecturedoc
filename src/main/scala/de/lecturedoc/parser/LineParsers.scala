/*
 * (c) 2013 Michael Eichberg et al.
 * https://bitbucket.org/delors/lecturedoc
 * 
 * See LICENSE and Actuarius-LICENSE for license details.
 */
package de.lecturedoc
package parser

import scala.util.matching.Regex

/**
  * Parses single lines into tokens.
  *
  * Markdown lines are differentiated by their beginning.
  * These lines are then organized in blocks by the BlockParsers.
  */
trait LineParsers extends InlineParsers {

    ////////////////////////
    // Helper for parsing //
    ////////////////////////

    /**
      * Regex for finding customDataAttributes at the end of a line.
      * Three spaces followed by at least one customDataAttribute enclosed in curly braces.
      * Additional optional customDataAttributes are separated by semicolons.
      */
    val customDataAttributesEOL: Regex = """ {3}\{(\w[\w\-_]*:\w+)(\;\w[\w\-_]*:\w+)*\}( {2})?$""".r

    /**
      * Regex for finding two whitespaces at the end of a line. 
      */
    val tws: Regex = """(  )$""".r

    /**
      * Parses customDataAttributes. customDataAttributes are surrounded by "   {" and "}".
      */
	val cdas: Parser[List[CustomDataAttribute]] = "   {" ~> customDataAttributes <~ '}'

    /////////////////////////////////
    // Link definition pre-parsing //
    /////////////////////////////////

    /**
      * The Start of a link definition: the id in square brackets, optionally indented by three spaces
      */
    def linkDefinitionId: Parser[String] =
        """ {0,3}\[""".r ~> markdownText(Set(']'), true) <~ ("]:" ~ ows) ^^ { _.trim.toLowerCase }

    /**
      * The link url in a link definition.
      */
    def linkDefinitionUrl: Parser[String] =
        (elem('<') ~> markdownText(Set('>'), true) <~ '>' ^^ { _.mkString.trim }) |
            (markdownText(Set(' ', '\t'), true) ^^ { _.mkString })

    /**
      * The title in a link definition.
      */
    def linkDefinitionTitle: Parser[String] =
        ows ~> ("""\"[^\n]*["]""".r |
            """\'[^\n]*\'""".r |
            """\([^\n]*\)""".r) <~ ows ^^ { s ⇒ s.substring(1, s.length - 1) }

    /**
      * A link definition that later gets stripped from the output.
      * Either a link definition on one line or the first line of a two line link definition.
      */
    def linkDefinitionStart: Parser[(LinkDefinitionStart, Option[String])] =
        linkDefinitionId ~ linkDefinitionUrl ~ opt(linkDefinitionTitle) ^^ { case i ~ u ~ t ⇒ (new LinkDefinitionStart(i, u), t) }

    //////////////////////////////////////////
    // Lines for XML Block tokenizing       //
    //////////////////////////////////////////

    /**
      * A line that starts an xml block: an opening xml element fragment.
      */
    def xmlBlockStartLine: Parser[String] = guard('<' ~ xmlName) ~> rest
    /**
      * A line that ends an xml block: a line starting with an xml end tag
      */
    def xmlBlockEndLine: Parser[String] = guard(xmlEndTag) ~> rest
    /**
      * A line not starting with an xml end tag
      */
    def notXmlBlockEndLine: Parser[String] = not(xmlEndTag) ~> rest

    //////////////////////////////
    // Markdown line tokenizing //
    //////////////////////////////

    /**
      * Parses the line under a setext style level 1 header: =====
      * Either followed by whitespace or by a customDataAttribute.
      */
    val setextHeader1: Parser[SetExtHeaderLine] = ("""=+([ \t]*)$""".r) ^^ { new SetExtHeaderLine(_, 1)
        } |
        ("""=+""".r) ~
        cdas ~
        opt(tws) ^^ {
            case header ~ customDataAttributes ~ br ⇒ new SetExtHeaderLine(header + br.getOrElse(""), 1, Option(customDataAttributes))
        }

    /**
      * Parses the line under a setext style level 2 header: -----
      * Either followed by whitespace or by a customDataAttribute.
      */
    val setextHeader2: Parser[SetExtHeaderLine] = ("""((\-)+)([ \t]*)$""".r) ^^ { new SetExtHeaderLine(_, 2)
        } |
        ("""((\-)+)""".r) ~
        cdas ~
        opt(tws) ^^ {
            case header ~ customDataAttributes ~ br ⇒ new SetExtHeaderLine(header + br.getOrElse(""), 2, Option(customDataAttributes))
        }

    /**
      * Parses headers of the form: ### header ###
      * Optionally followed by a customDataAttribute.
      */
    val atxHeader: Parser[AtxHeaderLine] = ("""#+""".r) ~
        restUntilRegex(customDataAttributesEOL) ~
        opt(cdas) ~
        opt(tws) ^^ {
            case prefix ~ payload ~ customDataAttributes ~ br ⇒ new AtxHeaderLine(prefix, payload + br.getOrElse(""), customDataAttributes)
        }

    /**
      * Parses statement of the form: ^statement, starting with a '^' and an optional whitespace if a payload is given.
      * Optionally followed by a customDataAttribute.
      */
    val statement: Parser[StatementLine] = ("""\^""".r) ~
        cdas ~ 
        opt(tws) ^^ {
            case prefix ~ customDataAttributes ~ br ⇒ new StatementLine(prefix, "" + br.getOrElse(""), Option(customDataAttributes))
        } |
        ("""\^( )?""".r) ~
        restUntilRegex(customDataAttributesEOL) ~
        opt(cdas) ~
        opt(tws) ^^ {
            case prefix ~ payload ~ customDataAttributes ~ br ⇒ new StatementLine(prefix, payload + br.getOrElse(""), customDataAttributes)
        }

    /**
      * Parses a horizontal rule.
      */
    val ruler: Parser[MarkdownLine] = """ {0,3}(((-[ \t]*){3,})|((\*[ \t]*){3,}))$""".r ^^ { new RulerLine(_) }

    /**
      * Matches a line starting with up to three spaces, a '>' and an optional whitespace if a payload is given.
      * (i.e.: the start or continuation of a block quote.)
      * Optionally followed by a customDataAttribute.
      */
    val blockquoteLine: Parser[BlockQuoteLine] = (""" {0,3}\>""".r) ~
        cdas ~
        opt(tws) ^^ {
            case prefix ~ customDataAttributes ~ br ⇒ new BlockQuoteLine(prefix, "" + br.getOrElse(""), Option(customDataAttributes))
        } |
        (""" {0,3}\>( )?""".r) ~
        restUntilRegex(customDataAttributesEOL) ~
        opt(cdas) ~
        opt(tws) ^^ {
            case prefix ~ payload ~ customDataAttributes ~ br ⇒ new BlockQuoteLine(prefix, payload + br.getOrElse(""), customDataAttributes)
        }

    /**
      * A line that starts an unordered list item.
      * Matches a line starting with up to three spaces followed by an asterisk, a space, and any whitespace.
      * Optionally followed by a customDataAttribute.
      *
      * If no payload is given, the asterisk has to be followed by three spaces and a customDataAttribute.
      * In this case, a break is inserted.
      */
    val uItemStartLine: Parser[UItemStartLine] = (""" {0,3}[\*\+-]""".r) ~
        cdas ~
        opt(tws) ^^ {
            case prefix ~ customDataAttributes ~ br ⇒ new UItemStartLine(prefix, "<br />", Option(customDataAttributes))
        } |
        (""" {0,3}[\*\+-] [\t\v ]*""".r) ~
        restUntilRegex(customDataAttributesEOL) ~
        opt(cdas) ~
        opt(tws) ^^ {
            case prefix ~ payload ~ customDataAttributes ~ br ⇒ new UItemStartLine(prefix, payload + br.getOrElse(""), customDataAttributes)
        }

    /**
      * A line that starts an ordered list item.
      * Matches a line starting with up to three spaces followed by a number, a dot and a space, and any whitespace.
      * Optionally followed by a customDataAttribute.
      *
      * If no payload is given, the dot has to be followed by three spaces and a customDataAttribute.
      */
    val oItemStartLine: Parser[OItemStartLine] = (""" {0,3}[0-9]+\.""".r) ~
        cdas ~ 
        opt(tws) ^^ {
            case prefix ~ customDataAttributes ~ br ⇒ new OItemStartLine(prefix, "" + br.getOrElse(""), Option(customDataAttributes))
        } |
        (""" {0,3}[0-9]+\. [\t\v ]*""".r) ~
        restUntilRegex(customDataAttributesEOL) ~
        opt(cdas) ~
        opt(tws) ^^ {
            case prefix ~ payload  ~ customDataAttributes ~ br ⇒ new OItemStartLine(prefix, payload + br.getOrElse(""), customDataAttributes)
        }

    /**
      * Accepts an empty line. (A line that consists only of optional whitespace or the empty string.)
      */
    val emptyLine: Parser[MarkdownLine] = """([ \t]*)$""".r ^^ { new EmptyLine(_) }

    /**
      * Matches a code example line: any line starting with four spaces or a tab.
      * Optionally followed by a customDataAttribute.
      */
    val codeLine: Parser[CodeLine] = ("    " | "\t") ~
        restUntilRegex(customDataAttributesEOL) ~
        opt(cdas) ~
        opt(tws) ^^ {
            case prefix ~ payload ~ customDataAttributes ~ br ⇒ new CodeLine(prefix, payload + br.getOrElse(""), customDataAttributes)
        }

    /**
      * A fenced code line. Can be the start or the end of a fenced code block:
      * Three or more backticks followed by a customDataAttribute
      * or by whitespace.
      */
    val fencedCodeLine: Parser[FencedCode] = ("""\`{3,}[\t\v ]*$""".r) ^^ {
            case prefix ⇒ new FencedCode(prefix)
        } |
        ("""\`{3,}""".r) ~
        cdas ~
        opt(tws) ^^ {
            case prefix ~ customDataAttributes ~ br ⇒ new FencedCode(prefix + br.getOrElse(""), Option(customDataAttributes))
        }

    /**
      * Matches the start of a fenced code block with additional language token:
      * Three or more backticks and optional whitespace followed by an optional
      * language token and optional whitespace or by an optional language token
      * and a customDataAttribute.
      */
    val extendedFencedCodeLine: Parser[ExtendedFencedCode] = ("""\`{3,}[\t\v ]*""".r) ~
        ("""[\w_-]+[\t\v ]*$""".r) ^^ {
            case prefix ~ languageToken ⇒ new ExtendedFencedCode(prefix, languageToken)
        } |
        ("""\`{3,}[\t\v ]*""".r) ~
        ("""[\w_-]+""".r) ~
        cdas ~
        opt(tws) ^^ {
            case prefix ~ languageToken ~ customDataAttributes ~ br ⇒ new ExtendedFencedCode(prefix, languageToken + br.getOrElse(""), Option(customDataAttributes))
        }

    def customDataAttributes: Parser[List[CustomDataAttribute]] =
        customDataAttribute ~ opt(';' ~> customDataAttributes) ^^ {
            case cda ~ Some(cdas) ⇒ cda +: cdas
            case cda ~ None       ⇒ List(cda)
        }

    def customDataAttribute: Parser[CustomDataAttribute] =
        """\w[\w\-_]*""".r ~ ':' ~ """\w+""".r ^^ {
            case key ~ _ ~ value ⇒ CustomDataAttribute(key, value)
        }

    /**
      * A section line starts with "+~" and optionally a title, class and
      * customDataAttribute.
      */
    val sectionStartLine: Parser[SectionStart] =
        """\+~""".r ~
            opt("[" ~> markdownText(Set(']'), true) <~ "]") ~
            opt("""([\w][\w\-_]*[\t\v ]*)+""".r) ~
            opt('{' ~> customDataAttributes <~ '}') ^^ {
                case prefix ~ sectionTitle ~ sectionClass ~ customDataAttributes ⇒
                    new SectionStart(prefix, sectionTitle, sectionClass, customDataAttributes)
            }

    /**
      * A section end line starts with "~+" followed by optional whitespace.
      */
    val sectionEndLine: Parser[SectionEnd] = """~\+[\t\v ]*$""".r ^^ {
        case prefix ⇒ new SectionEnd(prefix)
    }

    /**
      * Matches any line. Only called when all other line parsers have failed.
      * Makes sure line tokenizing does not fail and we do not loose any lines on the way.
      * Optionally followed by a customDataAttribute.
      */
    val otherLine: Parser[OtherLine] =
        restUntilRegex(customDataAttributesEOL) ~
        opt(cdas) ~
        opt(tws) ^^ {
            case payload ~ customDataAttributes ~ br ⇒ new OtherLine(payload + br.getOrElse(""), customDataAttributes)
        }

    ///////////////////////////////////////////////////////////////
    // combined parsers for faster tokenizing based on lookahead //
    ///////////////////////////////////////////////////////////////
    /**
      * First tries for a setext header level 2, then for a ruler.
      */
    val setext2OrRuler: Parser[MarkdownLine] = setextHeader2 | ruler
    /**
      * First tries for a ruler, then for an unordered list item start.
      */
    val rulerOrUItem: Parser[MarkdownLine] = ruler | uItemStartLine
    /**
      * First tries if the line is empty, if not tries for a code line.
      */
    val emptyOrCode: Parser[MarkdownLine] = emptyLine | codeLine

    /**
      * Parses one of the fenced code lines
      */
    val fencedCodeStartOrEnd: Parser[MarkdownLine] = fencedCodeLine | extendedFencedCodeLine

}