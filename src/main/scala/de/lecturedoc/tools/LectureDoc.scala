/*
 * (c) 2013 Michael Eichberg et al.
 * https://bitbucket.org/delors/lecturedoc
 * 
 * See LICENSE and Actuarius-LICENSE for license details. 
 */
package de.lecturedoc
package tools

import java.io.{ InputStream, OutputStream }
import java.util.Date
import java.text.DateFormat
import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.WatchKey
import java.nio.file.StandardWatchEventKinds.ENTRY_CREATE
import java.nio.file.StandardWatchEventKinds.OVERFLOW
import java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY

import de.lecturedoc.parser.DefaultLectureDocTransformer

/**
  * Contains a main method that simply reads everything from stdin, parses it as LectureDoc and
  * prints the result to stdout.
  */
object LectureDoc {

    /**
      * The help message. Loaded from the file "LectureDocHelp.txt" when needed.
      */
    private lazy val helpMessage =
        onCatch[Exception, String](
            withResource(this.getClass().getClassLoader().getResourceAsStream("LectureDocHelp.txt"))(
                scala.io.Source.fromInputStream(_).mkString
            )
        )("Help file not found. Please, go to http://bitbucket.org/delors/lecturedoc to report this error.")

    /**
      * Prints the help message and exits LectureDoc with the return code "-1" to indicate that
      * LectureDoc was not able to make sense out of the specified command line arguments.
      */
    private def printUsageAndExit() {
        println(helpMessage)
        System.exit(-1)
    }

    /**
      * Prints the given error message followed by the general help message. Afterwards, LectureDoc returns
      * with the return code "-2" to indicate that LectureDoc was not able to make sense out of the
      * specified command line arguments or was able to read the specified file.
      */
    private def printErrorAndExit(errorMessage: String) {
        System.err.println(errorMessage)
        println(helpMessage)
        System.exit(-2)
    }

    def main(args: Array[String]): Unit = {
        if (args.length > 3) printUsageAndExit();

        val complete = args.contains("--complete")
        val continuous = args.contains("--continuous")

        if (continuous) {
            // continuous mode. 
            // I.e., we watch a directory for changes and create the new LectureDoc files when needed
            import java.nio.file._
            import java.nio.file.StandardWatchEventKinds._
            import scala.collection.JavaConversions._

            val path: Path =
                if (complete && args.length == 3) { Paths.get(args(2)); }
                else if (!complete && args.length == 2) { Paths.get(args(1)); }
                else { Paths.get(System.getProperty("user.dir")) }

            if (!(Files.isWritable(path) && Files.isDirectory(path))) printUsageAndExit()

            println("Watching directory: "+path.toAbsolutePath()+" for changes.")

            val watchService = FileSystems.getDefault().newWatchService();
            path.register(watchService, ENTRY_CREATE, ENTRY_MODIFY);
            var watchKey: WatchKey = null
            do {
                watchKey = try { watchService.take() } catch { case e: InterruptedException ⇒ return }
                for {
                    event ← watchKey.pollEvents()
                    if event.kind() ne OVERFLOW
                    file = path.resolve(event.context().asInstanceOf[Path])
                    fileName = file.toString
                    if fileName.endsWith(".md")
                } {
                    // Convert the updated/new lecture doc file
                    val lastModified = Files.getLastModifiedTime(file).toMillis()
                    val date = new java.util.Date(lastModified)
                    println("Converting file: "+fileName+" last modified: "+DateFormat.getDateTimeInstance().format(date))
                    val newFileName = fileName.substring(0, fileName.length() - 3)+".html"
                    val newFile = Paths.get(newFileName)
                    Files.deleteIfExists(newFile);

                    var in: InputStream = null
                    var out: OutputStream = null

                    try {
                        in = Files.newInputStream(file)
                        out = Files.newOutputStream(newFile)
                        process(path.toFile(),in, DefaultLectureDocTransformer, out, complete, date)
                        Files.setLastModifiedTime(newFile, java.nio.file.attribute.FileTime.fromMillis(lastModified))
                    }
                    catch {
                        case e: java.io.IOException ⇒
                            System.err.println("Error while converting: "+file+" => "+newFile+" ("+e.getMessage()+")")
                    }
                    finally {
                        if (in != null)
                            in.close()
                        if (out != null)
                            out.close();
                    }
                }
            } while (watchKey.reset())

            System.err.println("Watching the path: "+path+" was aborted.")
        }
        else {
            // Standard mode
            if (args.length > 1) printErrorAndExit("Unsupported arguments: "+args.mkString(" "))
            process(
                    new java.io.File(System.getProperty("user.dir")),
                    System.in, 
                    DefaultLectureDocTransformer, 
                    System.out, 
                    complete, 
                    new Date() /*last-modified*/ )
        }
    }

}
