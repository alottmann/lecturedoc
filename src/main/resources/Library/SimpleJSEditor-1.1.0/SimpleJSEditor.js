/*
   Copyright 2012 Michael Eichberg et al - www.michael-eichberg.de

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

/**
 * <i>For instructions see SimpleJSEditor.html.</i>
 *
 * @author Michael Eichberg
 */
// "use strict"; cannot be used here, because it would prevent the evaluation of
// scripts that explicitly require a "non-strict" evaluation.
var SimpleJSEditor = function() {

    /* The code that was initially specified (as part of the HTML document). */
    var initialCode = [];
    /* The newest values from simplejseditor-editor, updated every time saveState is called. */
    var editorValues = [];
    /* The newest values from simplejseditor-result, updated every time saveState is called. */
    var resultValues = [];

    function editorElementId(instance) {
        return "simplejseditor-editor-"+instance;
    }

    function resultElementId(instance) {
        return "simplejseditor-result-"+instance;
    }

    function resultPaneElementId(instance) {
        return "simplejseditor-result-"+instance;
    }

    function evalInstance(instance,nonStrict){
        return function(){
            doEval(
                editorElementId(instance),
                resultPaneElementId(instance),
                nonStrict);
        };
    }

    function doEval(editorElementId,resultPaneElementId,nonStrict) {
        var js = document.getElementById(editorElementId).value;
        console.log("// Evaluating JavaScript");
        console.log(nonStrict ? '// "use strict";' : '"use strict";');
        console.log(js);

        // clear the result pane
        var resultElement = document.getElementById(resultPaneElementId);
        resultElement.value = "";

        // clear the previous result element, if it exists
        if (resultElement.parentNode.childNodes[1]) {
            resultElement.parentNode.removeChild(resultElement.parentNode.childNodes[1]);
        }

        /**
         * This function is intended to be used by scripts to output
         * something in the result view.
         * I.e., it is available in "eval()'s" scope.
         */
        function println() {
            if (arguments.length == 0){
                resultElement.value=resultElement.value+"\n";
            }
            for( var i = 0; i < arguments.length; i++ ) {
                resultElement.value=resultElement.value+arguments[i]+"\n";
            }
        };

        try {
            if(nonStrict === "" || nonStrict){
                var result = eval(js);
            } else {
                var result = function(){ "use strict"; return eval(js);}();
            }
            if (result !== undefined) {
                println(result);
                if(result instanceof Element){
                    resultElement.parentNode.appendChild(result);
                }
            }
        } catch (exception) {
            println("Catched: " + exception);
        }
    }


    return {
        create : function () {
            var allEditors = document.querySelectorAll('div.simplejseditor');
            var length = ( ( initialCode.length > 0 ) ? initialCode.length : allEditors.length );
            for(var i = 0; i < length ; i++){
                var root = allEditors.item(i);
                var cols = root.getAttribute("data-simplejseditor-cols") || 40;
                var rows = root.getAttribute("data-simplejseditor-rows") || 16;
                var resultCols = root.getAttribute("data-simplejseditor-result-cols") || cols;
                var resultRows = root.getAttribute("data-simplejseditor-result-rows") || rows;

                // Only set initialCode the first time this method is called
                if (initialCode[i] == null){
                    var content = "// use println() for output" // default text
                    if (root.childNodes.length > 0) {
                        content = root.childNodes[0].data || "// Invalid content in source document!";
                    }
                    if (content[0] === '\n'){ // Remove the first newline if the code block starts with just a newline
                        content = content.slice(1);
                    }
                    initialCode[i] = content;
                    editorValues[i] = content;
                }

                while (root.childNodes.length > 0) {
                    root.removeChild(root.childNodes[0]);
                }

                var editorDiv = document.createElement("div");
                editorDiv.className = "simplejseditor-editor";
                root.appendChild(editorDiv);
                var editorTextarea = document.createElement("textarea");
                editorTextarea.addEventListener("keyup",function(event){
                    event.stopPropagation();
                },false);
                editorTextarea.addEventListener("touchstart",function(event){
                    event.stopPropagation();
                },false);
                editorTextarea.addEventListener("touchend",function(event){
                    event.stopPropagation();
                },false);
                editorTextarea.value = editorValues[i];
                editorTextarea.id = editorElementId(i);
                editorTextarea.rows = rows;
                editorTextarea.cols = cols;
                editorDiv.appendChild(editorTextarea);

                var resultDiv = document.createElement("div");
                resultDiv.className = "simplejseditor-result";
                root.appendChild(resultDiv);
                var resultTextarea = document.createElement("textarea");
                resultTextarea.id = resultPaneElementId(i);
                resultTextarea.rows = resultRows;
                resultTextarea.cols = resultCols;
                resultTextarea.readOnly = true;
                if (resultValues[i] != null){
                    resultTextarea.value = resultValues[i]
                }
                resultDiv.appendChild(resultTextarea);

                var footer = document.createElement("div");
                footer.className = "simplejseditor-footer";
                root.appendChild(footer);

                var evalButton = document.createElement("button");
                evalButton.className = "simplejseditor-eval-button";
                evalButton.innerHTML = "eval(…)";
                footer.appendChild(evalButton);
                var evalFunc = evalInstance(i,root.getAttribute("data-simplejseditor-non-strict"));
                evalButton.onclick = evalFunc;

                evalButton.addEventListener("touchstart",function(event){
                    event.stopPropagation();
                },false);
                evalButton.addEventListener("touchend",function(event){
                    event.stopPropagation();
                },false);
                var resetButton = document.createElement("button");
                resetButton.className = "simplejseditor-reset-button";
                resetButton.innerHTML = "reset";
                footer.appendChild(resetButton);
                resetButton.onclick =
                    function(editorElement,resultElement,code){
                        return function(){
                            editorElement.value = code;
                            resultElement.value = "";
                        };
                    }(editorTextarea,resultTextarea,editorValues[i]);
                resetButton.addEventListener("touchstart",function(event){
                    event.stopPropagation();
                },false);
                resetButton.addEventListener("touchend",function(event){
                    event.stopPropagation();
                },false);
                var evalOnLoad = root.getAttribute("data-simplejseditor-eval");
                if(evalOnLoad === "" || evalOnLoad) {
                    evalFunc();
                }
            }
        },

    saveState : function() {
            var allEditors = document.querySelectorAll('div.simplejseditor');
            for(var i = 0; i < initialCode.length ; i++){
                if (initialCode[i] != null){
                    editorValues[i] = document.getElementById(editorElementId(i)).value;
                    var resultValue = document.getElementById(resultElementId(i)).value;
                    if (resultValue != null){
                        resultValues[i] = resultValue;
                    }
                }
            }
        }
    };
}();
