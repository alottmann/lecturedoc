#!/bin/bash
# 
# requires:
# * 'wkhtmltopdf' to generate a pdf
# * 'lecturedoc' to convert the md file to html (this should be a script that simply wraps lecturedoc-*-one-jar.jar)
# * 'sed' to automatically insert custom css into the generated html
#
# for an explanation of the print_* variables, see wkhtmltopdf documentation

src=QS.md
html=${src%.*}.html
pdf=${src%.*}.pdf
print_margins='-B 22 -L 18 -R 12 -T 18'
print_header=header.html
print_footer=footer.html
css=template.css
files="$src $print_header $print_footer $css"

if [[ $1 == loop ]]; then
	while true; do
		inotifywait -e modify,close_write $files &&
			lecturedoc --complete <$src >$html &&
			sed -i '1a\<link href="'$css'" rel="stylesheet" type="text/css" />' $html &&
			wkhtmltopdf --footer-html $print_footer --header-html $print_header --print-media-type $print_margins $html $pdf
	done
else
	lecturedoc --complete <$src >$html &&
		sed -i '1a\<link href="'$css'" rel="stylesheet" type="text/css" />' $html &&
		wkhtmltopdf --footer-html $print_footer --header-html $print_header --print-media-type $print_margins $html $pdf
fi
