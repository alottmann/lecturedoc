#LectureDoc

+~[LectureDoc]slide

^![Logo](Library/LectureDoc/Logo@0.25x.png) **LectureDoc**

<div style="text-align: center; margin-top: 5.5em;">
<p><strong>Team 22</strong></p>
<ul style="list-style-type: none; padding: 0; margin: 0;">
<li>Daniel Killer
<li>Arne Lottmann 
<li>Kerstin Reifschläger 
<li>Simone Wälde
</ul>
<p style="margin-top: 2em">05.07.2013</p>
</div>

~+

+~[LectureDoc]slide
#LectureDoc
  
  <p></p>
  
^**Ziel:** Folien und Skript aus **einem** Quelldokument
  
  <p></p>

1. **Markdown-Dialekt**

	Syntaxerweiterungen für Folien
  
2. **Parser**

	Transformiert LectureDoc-Quelldateien nach HTML

3. **JavaScript & CSS**

	Erlaubt Darstellung in verschiedenen Modi

~+

zu Ziel

* "Ihr kennt das ja…" Folien vs. Skript
* Bereits existierende Anwendung
* Hauptaufgaben: Modernisierung der Oberfläche / Implementierung fehlender Features

zu Markdown

* Auszeichnungssprache ähnlich wie Latex
* Quelldokumentsprache

zu JavaScript

**DEMO**

Hier: Zurück zum Aufhänger (Folie vs Skript)

+~[Markdown]slide

#Markdown

	## Lorem ipsum

	* **dolor sit amet**
	    * *consetetur* sadipscing

	----

	1. sed diam nomumy
	2. eirmod tempor

  <p></p>

## Lorem ipsum
* **dolor sit amet**
	* *consetetur* sadipscing

----

1. sed diam nomumy
2. eirmod tempor

~+

+~[Parser]slide

#Parser

  <p></p>
  
Geschrieben in Scala

^![Parser](Parser.png)

~+

+~[Besondere Features]slide

^**Features**

~+

+~[Asides]slide

#Asides

Einbettung von Zusatzinformationen in Präsentationsmodus

+~[klick]aside
Aside-Informationen werden per Mausklick aufgerufen und erscheinen in einer ansprechenden grauen Box mit runden Ecken.
~+

~+

Box mit Zusatzinformationen erscheint über dem Folieninhalt

+~[Timeline]slide

#Timeline

	<svg id="JavaScript-Timeline" class="timeline" />
	<script type="text/javascript">
		{d:"03/1996",t:"1.0"},
		{d:"08/1996",t:"1.1"},
		{d:"06/1997",t:"1.2"},
		{d:"10/1998",t:"1.3 - ECMA-262 1st + 2nd edition"},
		{d:"11/2000",t:"1.5 - ECMA-262 3rd edition"},						
		{d:"11/2005",t:"1.6"},
		{d:"10/2006",t:"1.7"},
		{d:"06/2008",t:"1.8"},
		{d:"07/2010",t:"1.8.5 - ECMAScript 5"}
	]);
	</script>

<svg id="JavaScript-Timeline" class="timeline" />
<script type="text/javascript">
	Timeline.draw("JavaScript-Timeline",[
		{d:"03/1996",t:"1.0"},
		{d:"08/1996",t:"1.1"},
		{d:"06/1997",t:"1.2"},
		{d:"10/1998",t:"1.3 - ECMA-262 1st + 2nd edition"},
		{d:"11/2000",t:"1.5 - ECMA-262 3rd edition"},						
		{d:"11/2005",t:"1.6"},
		{d:"10/2006",t:"1.7"},
		{d:"06/2008",t:"1.8"},
		{d:"07/2010",t:"1.8.5 - ECMAScript 5"}
	]);
</script>

~+

+~[Multiple Choice]slide

#Multiple Choice

	<div id="question"></div>
	<script type="text/javascript">
		MultipleChoice.showQuestion("question", "Vielfache von drei sind…", [
			{a: "7", c: false, e: "7=3&times;2 + 1 \u2717"},
			{a: "9", c: true, e: "9=3&times;3 \u2713"},
			{a: "211", c: false, e: "211=70&times;3 + 1 \u2717"},
			{a: "2511", c: true, e: "2511=837&times;3 \u2713"}
		]);
	</script>

<div id="question"></div>
<script type="text/javascript">
	MultipleChoice.showQuestion("question", "Vielfache von drei sind…", [
		{a: "7", c: false, e: "7=3&times;2 + 1 \u2717"},
		{a: "9", c: true, e: "9=3&times;3 \u2713"},
		{a: "211", c: false, e: "211=70&times;3 + 1 \u2717"},
		{a: "2511", c: true, e: "2511=837&times;3 \u2713"}
	]);
</script>

~+

Wir haben das gemacht

+~[Ausblick]slide

#Ausblick

  <p></p>

* Menüleiste

* Animationen

* Tablets

~+

zu Menü

* macht Kommandos grafisch zugänglich 

zu Animationen

* Übergänge zwischen Folien
* Elemente der Reihe nach per Mausklick erscheinen lassen

+~[Qualitätssicherung]slide

^**Qualitätssicherung**

* **Portabilität**

* **Wartbarkeit**

~+

+~[Portabilität]slide

#Portabilität

**Zielplattformen**
* Browser: Chrome/Firefox/Safari (aktuelle Versionen ab 18. August 2013)
* Backend: Windows 7/Mac OS 10.8.4/Linux Kernel 3.9.8

  <p></p>

**Maßnahmen**
* Regelmäßige manuelle Tests
* Automatische Tests mit Selenium

~+

zu Zielplattformen

* Rechner: Betriebssysteme, die uns zur Verfügung stehen

+~[Wartbarkeit]slide

#Wartbarkeit

Entwicklung wird weitergehen ⇨ leicht wartbarer Code gefordert

  <p></p>

**Maßnahmen**
* Formatierung durch automatische Tools 
* Code Reviews am Ende jeder Iteration  
  
	⇨ Konsistente Benennung  
	⇨ Aktuelle Dokumentation

~+

+~[Zeiten]slide

^**Zeiten**

~+

+~[Erbrachte Zeiten]slide
#Erbrachte Zeiten pro Person

  <p style="position: absolute; top: 6em; right: 8em; font-weight: bold;">Gesamt: 258.2h</p>

![Zeitanteile](Gruppenzeiten.png)

~+

Leichte Varianz, weil Leute unterschiedlich viel Zeit aufwenden konnten

+~[Erbrachte Zeiten]slide
#Story Points und Velocity

![Story Points und Velocity](StoryPointsZeiten.png)

~+

Velocity:

* Ziel: 1 Std./SP
* Story Points am Anfang stark überschätzt
* Später User Stories nicht abgenommen wegen Ansprüchen an Codequalität

+~[Erbrachte Zeiten]slide
#Zeiten pro Iteration

![Zeiten pro Iteration](ZeitProIteration.png)

~+

* Anfangs mehr Einarbeitung (neue Programmiersprache(n)) und organisatorische Meetings
* Iterationslänge: 2 Wochen

+~[Fragen]slide

^**Noch Fragen?**

<div style="width: 100%; text-align:center;">
<img src="NochFragen.png">
</div>

~+

+~[Vielen Dank]slide

^**Vielen Dank für Ihre Aufmerksamkeit**

~+
